package de.geoapis.apps.mv.prefs;


import android.content.SharedPreferences;
import com.google.android.gms.maps.model.CameraPosition;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.Injector;
import info.metadude.android.typedpreferences.IntPreference;
import info.metadude.android.typedpreferences.StringPreference;

import javax.inject.Inject;

public class PreferencesHelper {

    @Inject
    SharedPreferences mSharedPreferences;

    protected static final String PREF_KEY_MAP_TYPE_INDEX =
            BuildConfig.APPLICATION_ID + ".MAP_TYPE_INDEX";

    protected static final String PREF_KEY_MAP_CAMERA_POSITION =
            BuildConfig.APPLICATION_ID + ".MAP_CAMERA_POSITION";

    protected static final String PREF_KEY_USER_NAME =
            BuildConfig.APPLICATION_ID + ".USER_NAME";

    protected static final String PREF_KEY_EMAIL_ADDRESS =
            BuildConfig.APPLICATION_ID + ".EMAIL_ADDRESS";

    protected final IntPreference mMapTypeIndexPreference;

    protected final CameraPositionPreference mCameraPositionPreference;

    protected final StringPreference mUserNamePreference;

    protected final StringPreference mEmailAddressPreference;


    public PreferencesHelper() {
        Injector.inject(this);
        mMapTypeIndexPreference = new IntPreference(
                mSharedPreferences, PREF_KEY_MAP_TYPE_INDEX);
        mCameraPositionPreference = new CameraPositionPreference(
                mSharedPreferences, PREF_KEY_MAP_CAMERA_POSITION);
        mUserNamePreference = new StringPreference(
                mSharedPreferences, PREF_KEY_USER_NAME);
        mEmailAddressPreference = new StringPreference(
                mSharedPreferences, PREF_KEY_EMAIL_ADDRESS);
    }

    // Map type index

    public void storeMapTypeIndex(int mapTypeIndex) {
        mMapTypeIndexPreference.set(mapTypeIndex);
    }

    public int restoreMapTypeIndex() {
        return mMapTypeIndexPreference.get();
    }

    public boolean storesMapTypeIndex() {
        return mMapTypeIndexPreference.isSet();
    }

    // Camera position

    public void storeCameraPosition(final CameraPosition cameraPosition) {
        mCameraPositionPreference.set(cameraPosition);
    }

    public CameraPosition restoreCameraPosition() {
        return mCameraPositionPreference.get();
    }

    public boolean storesCameraPosition() {
        return mCameraPositionPreference.isSet();
    }

    // User name

    public void storeUserName(final String userName) {
        mUserNamePreference.set(userName);
    }

    public String restoreUserName() {
        return mUserNamePreference.get();
    }

    public boolean storesUserName() {
        return mUserNamePreference.isSet();
    }

    // Email address

    public void storeEmailAddress(final String emailAddress) {
        mEmailAddressPreference.set(emailAddress);
    }

    public String restoreEmailAddress() {
        return mEmailAddressPreference.get();
    }

    public boolean storesEmailAddress() {
        return mEmailAddressPreference.isSet();
    }

}
