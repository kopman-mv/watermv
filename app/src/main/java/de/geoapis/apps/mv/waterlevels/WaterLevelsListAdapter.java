package de.geoapis.apps.mv.waterlevels;

import android.content.Context;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.data.model.ReadGaugeReportsResponse;
import org.joda.time.DateTime;

import java.util.List;

public class WaterLevelsListAdapter extends RecyclerView.Adapter<WaterLevelViewHolder> {

    private final SortedList<ReadGaugeReportsResponse> mGaugeReports;

    private Context mContext;

    public WaterLevelsListAdapter(List<ReadGaugeReportsResponse> gaugeReports) {
        mGaugeReports = new SortedList<>(
                ReadGaugeReportsResponse.class,
                new SortedListAdapterCallback<ReadGaugeReportsResponse>(this) {

                    @Override
                    public int compare(ReadGaugeReportsResponse object1,
                                       ReadGaugeReportsResponse object2) {
                        if (object1.getRecordedAt().equals(object2.getRecordedAt())) {
                            return 0;
                        }
                        DateTime recordedAt1 = object1.getRecordedAtAsDateTime();
                        DateTime recordedAt2 = object2.getRecordedAtAsDateTime();
                        // Latest date should appear on the top
                        return recordedAt2.compareTo(recordedAt1);
                    }

                    @Override
                    public boolean areContentsTheSame(ReadGaugeReportsResponse oldItem,
                                                      ReadGaugeReportsResponse newItem) {
                        return oldItem.getRecordedAt().equals(newItem.getRecordedAt()) &&
                                oldItem.getWaterLevel() == newItem.getWaterLevel() &&
                                oldItem.getUserName().equals(newItem.getUserName());
                    }

                    @Override
                    public boolean areItemsTheSame(ReadGaugeReportsResponse item1,
                                                   ReadGaugeReportsResponse item2) {
                        return item1.getRecordedAt().equals(item2.getRecordedAt()) &&
                                item1.getWaterLevel() == item2.getWaterLevel() &&
                                item1.getUserName().equals(item2.getUserName());
                    }
                });
        for (ReadGaugeReportsResponse gaugeReport : gaugeReports) {
            mGaugeReports.add(gaugeReport);
        }
    }

    @Override
    public WaterLevelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        mContext = parent.getContext();
        View itemView = layoutInflater.inflate(R.layout.gauge_report_item, parent, false);
        return new WaterLevelViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WaterLevelViewHolder holder, int position) {
        if (mGaugeReports != null) {
            holder.updateContent(mContext, mGaugeReports.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mGaugeReports == null ? 0 : mGaugeReports.size();
    }

}
