package de.geoapis.apps.mv.waterlevels;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import butterknife.InjectView;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.data.model.ReadGaugeReportsResponse;

import java.util.ArrayList;
import java.util.List;

public class WaterLevelsListFragment extends WaterLevelsFragment {

    public static final String FRAGMENT_TAG =
            BuildConfig.APPLICATION_ID + ".WATER_LEVELS_LIST_FRAGMENT_TAG";

    @InjectView(R.id.water_levels_list)
    RecyclerView mRecyclerView;

    @InjectView(R.id.water_levels_list_empty)
    View mWaterLevelsListEmptyView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_water_levels_list, container, false);
        inject(rootView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.reports_list, menu);
    }

    @Override
    protected void updateView() {
        if ((mCrowdSourcingGaugeReports == null || mCrowdSourcingGaugeReports.isEmpty()) &&
                (mRealTimeGaugeReports == null || mRealTimeGaugeReports.isEmpty())) {
            mRecyclerView.setVisibility(View.GONE);
            mWaterLevelsListEmptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mWaterLevelsListEmptyView.setVisibility(View.GONE);
            renderList();
        }
    }

    private void renderList() {
        List<ReadGaugeReportsResponse> gaugeReports = new ArrayList<>();
        if (mCrowdSourcingGaugeReports != null) {
            gaugeReports.addAll(mCrowdSourcingGaugeReports);
        }
        if (mRealTimeGaugeReports != null) {
            gaugeReports.addAll(mRealTimeGaugeReports);
        }
        if (mHistoricGaugeReports != null) {
            gaugeReports.addAll(mHistoricGaugeReports);
        }
        WaterLevelsListAdapter waterLevelsListAdapter = new WaterLevelsListAdapter(gaugeReports);
        mRecyclerView.setAdapter(waterLevelsListAdapter);
    }

}
