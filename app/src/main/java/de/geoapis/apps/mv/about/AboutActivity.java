package de.geoapis.apps.mv.about;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;

import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.base.BaseActivity;

public class AboutActivity extends BaseActivity {

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        WebView aboutView = (WebView) findViewById(R.id.about_web_view);
        aboutView.getSettings().setJavaScriptEnabled(true);
        aboutView.loadUrl("file:///android_asset/about.html");
    }

}
