package de.geoapis.apps.mv.data.model.streams;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.parceler.Parcel;
import org.parceler.Transient;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

@Parcel
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "geo",
        "id",
        "properties",
        "type"
})
public class Feature {

    @JsonProperty("geo")
    public Geo geo;
    @JsonProperty("id")
    public int id;
    @JsonProperty("properties")
    public Properties properties;
    @JsonProperty("type")
    public String type;

    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Feature() {
        // Required by Parceler
    }

    /**
     * @return The geo
     */
    @JsonProperty("geo")
    public Geo getGeo() {
        return geo;
    }

    /**
     * @param geo The geo
     */
    @JsonProperty("geo")
    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    /**
     * @return The id
     */
    @JsonProperty("id")
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The properties
     */
    @JsonProperty("properties")
    public Properties getProperties() {
        return properties;
    }

    /**
     * @param properties The properties
     */
    @JsonProperty("properties")
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
