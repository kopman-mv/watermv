package de.geoapis.apps.mv.prefs;

import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

import info.metadude.android.typedpreferences.DoublePreference;

public class LatLngPreference {

    protected final SharedPreferences mSharedPreferences;
    protected final DoublePreference mLatitudePreference;
    protected final DoublePreference mLongitudePreference;

    public LatLngPreference(final SharedPreferences sharedPreferences, final String key) {
        mSharedPreferences = sharedPreferences;
        String keyLatitude = key + ".LATITUDE";
        String keyLongitude = key + ".LONGITUDE";
        mLatitudePreference = new DoublePreference(mSharedPreferences, keyLatitude);
        mLongitudePreference = new DoublePreference(mSharedPreferences, keyLongitude);
    }

    public LatLng get() {
        double latitude = mLatitudePreference.get();
        double longitude = mLongitudePreference.get();
        return new LatLng(latitude, longitude);
    }

    public boolean isSet() {
        return mLatitudePreference.isSet() && mLongitudePreference.isSet();
    }

    public void set(final LatLng latLng) {
        mLatitudePreference.set(latLng.latitude);
        mLongitudePreference.set(latLng.longitude);
    }

    public void delete() {
        mLatitudePreference.delete();
        mLongitudePreference.delete();
    }

}
