package de.geoapis.apps.mv.map;

import com.google.android.gms.maps.model.TileProvider;
import de.geoapis.apps.mv.BuildConfig;


public class MagunTileProviderFactory {

    enum RESOLUTION {
        ROUGH,
        MEDIUM,
        FINE
    }

    public static TileProvider getMagunTileProvider(RESOLUTION resolution) {
        switch (resolution) {
            case ROUGH:
                return new MagunTileProvider(
                        BuildConfig.TILE_PROVIDER_ROUGH_API_URL,
                        BuildConfig.TILE_PROVIDER_ROUGH_MIN_ZOOM,
                        BuildConfig.TILE_PROVIDER_ROUGH_MAX_ZOOM);
            case MEDIUM:
                return new MagunTileProvider(
                        BuildConfig.TILE_PROVIDER_MEDIUM_API_URL,
                        BuildConfig.TILE_PROVIDER_MEDIUM_MIN_ZOOM,
                        BuildConfig.TILE_PROVIDER_MEDIUM_MAX_ZOOM);
            case FINE:
                return new MagunTileProvider(
                        BuildConfig.TILE_PROVIDER_FINE_API_URL,
                        BuildConfig.TILE_PROVIDER_FINE_MIN_ZOOM,
                        BuildConfig.TILE_PROVIDER_FINE_MAX_ZOOM);
            default:
                throw new IllegalStateException("Unknown tile provider resolution: " + resolution);
        }
    }

}
