package de.geoapis.apps.mv.devinfo;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.base.BaseActivity;

public class DeveloperInformationActivity extends BaseActivity {

    @InjectView(R.id.developer_info_version_code)
    TextView mVersionCodeTextView;

    @InjectView(R.id.developer_info_version_name)
    TextView mVersionNameTextView;

    @InjectView(R.id.developer_info_build_time)
    TextView mBuildTimeTextView;

    @InjectView(R.id.developer_info_git_hash)
    TextView mGitHashTextView;

    @InjectView(R.id.developer_info_package_name)
    TextView mPackageNameTextView;

    @InjectView(R.id.developer_info_android_version)
    TextView mAndroidVersionTextView;

    @InjectView(R.id.developer_info_gauge_features_api_end_point)
    TextView mGaugeFeaturesApiEndPointTextView;

    @InjectView(R.id.developer_info_gauge_reports_api_end_point)
    TextView mGaugeReportsApiEndPointTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer_information);
        ButterKnife.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpDeveloperInformation();
    }

    private void setUpDeveloperInformation() {
        String text = getString(R.string.developer_info_version_code, BuildConfig.VERSION_CODE);
        mVersionCodeTextView.setText(text);

        text = getString(R.string.developer_info_version_name, BuildConfig.VERSION_NAME);
        mVersionNameTextView.setText(text);

        text = getString(R.string.developer_info_build_time, BuildConfig.BUILD_TIME);
        mBuildTimeTextView.setText(text);

        text = getString(R.string.developer_info_git_hash, BuildConfig.GIT_HASH);
        mGitHashTextView.setText(text);

        text = getString(R.string.developer_info_package_name, BuildConfig.APPLICATION_ID);
        mPackageNameTextView.setText(text);

        text = getString(R.string.developer_info_android_version,
                Build.VERSION.RELEASE, Build.VERSION.SDK_INT);
        mAndroidVersionTextView.setText(text);

        text = getString(R.string.developer_info_gauge_features_api_end_point,
                BuildConfig.GAUGE_FEATURES_API_END_POINT);
        mGaugeFeaturesApiEndPointTextView.setText(text);

        text = getString(R.string.developer_info_gauge_reports_api_end_point,
                BuildConfig.GAUGE_REPORTS_API_END_POINT);
        mGaugeReportsApiEndPointTextView.setText(text);
    }

}
