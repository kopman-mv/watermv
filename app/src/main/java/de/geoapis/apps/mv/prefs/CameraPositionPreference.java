package de.geoapis.apps.mv.prefs;

import android.content.SharedPreferences;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import info.metadude.android.typedpreferences.FloatPreference;

public class CameraPositionPreference {

    protected final SharedPreferences mSharedPreferences;

    protected final LatLngPreference mTargetPreference;
    protected final FloatPreference mZoomPreference;
    protected final FloatPreference mTiltPreference;
    protected final FloatPreference mBearingPreference;

    public CameraPositionPreference(final SharedPreferences sharedPreferences, final String key) {
        mSharedPreferences = sharedPreferences;
        String keyZoom = key + ".ZOOM";
        String keyTilt = key + ".TILT";
        String keyBearing = key + ".BEARING";
        mTargetPreference = new LatLngPreference(mSharedPreferences, key);
        mZoomPreference = new FloatPreference(mSharedPreferences, keyZoom);
        mTiltPreference = new FloatPreference(mSharedPreferences, keyTilt);
        mBearingPreference = new FloatPreference(mSharedPreferences, keyBearing);
    }

    public CameraPosition get() {
        LatLng target = mTargetPreference.get();
        float zoom = mZoomPreference.get();
        float tilt = mTiltPreference.get();
        float bearing = mBearingPreference.get();
        return new CameraPosition(target, zoom, tilt, bearing);
    }

    public boolean isSet() {
        return mTargetPreference.isSet() &&
                mZoomPreference.isSet() &&
                mTiltPreference.isSet() &&
                mBearingPreference.isSet();
    }

    public void set(final CameraPosition cameraPosition) {
        mTargetPreference.set(cameraPosition.target);
        mZoomPreference.set(cameraPosition.zoom);
        mTiltPreference.set(cameraPosition.tilt);
        mBearingPreference.set(cameraPosition.bearing);
    }

    public void delete() {
        mTargetPreference.delete();
        mZoomPreference.delete();
        mTiltPreference.delete();
        mBearingPreference.delete();
    }

}
