package de.geoapis.apps.mv.data.api;

import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.data.model.gauges.ReadGaugeFeaturesResponse;
import de.geoapis.apps.mv.data.model.streams.ReadStreamFeaturesResponse;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface GaugeFeaturesService {

    @GET(BuildConfig.GAUGE_FEATURES_API_READ_PATH)
    public Observable<ReadGaugeFeaturesResponse> readGaugeFeatures(
            @Path("federalState") FederalState federalState,
            @Query("filter") String filter);

    @GET(BuildConfig.GAUGE_FEATURES_API_READ_PATH)
    public Observable<ReadStreamFeaturesResponse> readStreamFeatures(
            @Path("federalState") FederalState federalState,
            @Query("filter") String filter);
}
