package de.geoapis.apps.mv.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

public abstract class DateTimeFormatter {

    public static String getFormattedDateTime(String dateTime, String pattern) {
        return getFormattedDateTime(new DateTime(dateTime), pattern);
    }

    public static String getFormattedDateTime(DateTime dateTime, String pattern) {
        org.joda.time.format.DateTimeFormatter dateTimeFormatter =
                DateTimeFormat.forPattern(pattern);
        return dateTimeFormatter.withZone(DateTimeZone.getDefault()).print(dateTime);
    }

}
