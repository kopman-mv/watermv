package de.geoapis.apps.mv.reports;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.InjectView;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.contract.BundleKeys;
import de.geoapis.apps.mv.customviews.CustomDatePicker;
import de.geoapis.apps.mv.customviews.CustomTimePicker;
import org.joda.time.DateTime;

public class DateTimePickerFragment extends DialogFragment {

    public static final String FRAGMENT_TAG =
            BuildConfig.APPLICATION_ID + ".DATE_TIME_PICKER_FRAGMENT_TAG";
    public static final String DATE_TIME_PICKED_INTENT_KEY =
            BuildConfig.APPLICATION_ID + ".DATE_TIME_PICKED_INTENT_KEY";
    public static final int DATE_TIME_PICKED_RESULT_CODE = 100;

    @InjectView(R.id.date_picker)
    CustomDatePicker mDatePicker;

    @InjectView(R.id.time_picker)
    CustomTimePicker mTimePicker;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        FragmentActivity activity = getActivity();
        View contentView = activity.getLayoutInflater()
                .inflate(R.layout.fragment_date_time_picker, null);
        ButterKnife.inject(this, contentView);

        Long dateTimeInMilliSeconds = getArguments().getLong(BundleKeys.DATE_TIME_IN_MILLISECONDS);
        DateTime recordedAt = new DateTime(dateTimeInMilliSeconds);
        initializeControls(recordedAt);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                new ContextThemeWrapper(activity, R.style.AppTheme_Dialog));
        dialogBuilder
                .setView(contentView)
                .setTitle(R.string.date_time_picker_title)
                .setPositiveButton(R.string.date_time_picker_positive, (dialog, which) -> {
                    // Pass date and time to caller
                    passBackDateTime();
                })
                .setNegativeButton(R.string.date_time_picker_negative, (dialog, which) -> {
                    // Nothing to do here
                });
        return dialogBuilder.create();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    private void passBackDateTime() {
        DateTime dateTime = getDateTimeFromControls();
        Intent intent = new Intent();
        intent.putExtra(DATE_TIME_PICKED_INTENT_KEY, dateTime.getMillis());
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), DATE_TIME_PICKED_RESULT_CODE, intent);
    }

    private DateTime getDateTimeFromControls() {
        return new DateTime(
                mDatePicker.getYear(),
                mDatePicker.getMonth() + 1,
                mDatePicker.getDayOfMonth(),
                mTimePicker.getCurrentHour(),
                mTimePicker.getCurrentMinute()
        );
    }

    private void initializeControls(DateTime dateTime) {
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear() - 1;
        int day = dateTime.getDayOfMonth();
        int hour = dateTime.getHourOfDay();
        int minute = dateTime.getMinuteOfHour();

        mDatePicker.updateDate(year, month, day);
        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(minute);
        mTimePicker.setIs24HourView(true);
    }

}
