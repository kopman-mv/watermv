package de.geoapis.apps.mv.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class BoundingBox {

    public static double HALF_BOUNDING_BOX_SIZE = 0.00001d;

    protected LatLng mNorthEast;

    protected LatLng mSouthWest;

    public BoundingBox(double northEastLatitude,
                       double northEastLongitude,
                       double southWestLatitude,
                       double southWestLongitude) {
        mNorthEast = new LatLng(northEastLatitude, northEastLongitude);
        mSouthWest = new LatLng(southWestLatitude, southWestLongitude);
    }

    public BoundingBox(final LatLngBounds latLngBounds) {
        mNorthEast = latLngBounds.northeast;
        mSouthWest = latLngBounds.southwest;
    }

    public LatLng getNorthEast() {
        return mNorthEast;
    }

    public LatLng getSouthWest() {
        return mSouthWest;
    }

    public static BoundingBox createBoundingBox(final LatLng location) {
        return new BoundingBox(
                location.latitude + HALF_BOUNDING_BOX_SIZE,
                location.longitude + HALF_BOUNDING_BOX_SIZE,
                location.latitude - HALF_BOUNDING_BOX_SIZE,
                location.longitude - HALF_BOUNDING_BOX_SIZE);
    }

}
