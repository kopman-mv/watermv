package de.geoapis.apps.mv.data.model;

import org.joda.time.DateTime;

public final class HistoricGaugeReport implements
        ReadGaugeReportsResponse {

    public static final String USER_NAME = "LUNG-MV";

    protected final long mTimeStamp;

    protected final DateTime mTimeStampAsDateTime;

    protected final int mWaterLevel;

    public HistoricGaugeReport(long timeStamp, long waterLevel) {
        mTimeStamp = timeStamp;
        mTimeStampAsDateTime = new DateTime(timeStamp);
        mWaterLevel = (int) waterLevel;
    }

    @Override
    public int getWaterLevel() {
        return mWaterLevel;
    }

    @Override
    public String getUserName() {
        return USER_NAME;
    }

    @Override
    public String getGaugePhoto() {
        return null;
    }

    @Override
    public String getRecordedAt() {
        return "" + mTimeStamp;
    }

    @Override
    public DateTime getRecordedAtAsDateTime() {
        return mTimeStampAsDateTime;
    }

}
