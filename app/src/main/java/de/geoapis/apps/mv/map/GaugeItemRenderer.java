package de.geoapis.apps.mv.map;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import de.geoapis.apps.mv.map.model.GaugeItem;

public class GaugeItemRenderer extends DefaultClusterRenderer<GaugeItem> {

    public GaugeItemRenderer(final Context context,
                             final GoogleMap map,
                             final ClusterManager<GaugeItem> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(
            final GaugeItem gaugeItem, final MarkerOptions markerOptions) {
        markerOptions.title(gaugeItem.getGaugeName());
    }

}
