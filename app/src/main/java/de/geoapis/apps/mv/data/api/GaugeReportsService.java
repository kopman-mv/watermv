package de.geoapis.apps.mv.data.api;

import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.data.model.CreateCrowdSourcingGaugeReportResponse;
import de.geoapis.apps.mv.data.model.ReadCrowdSourcingGaugeReportsResponse;
import retrofit.http.*;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import rx.Observable;

import java.util.List;

public interface GaugeReportsService {

    @GET(BuildConfig.CROWD_SOURCING_GAUGE_REPORTS_API_READ_PATH)
    public Observable<List<ReadCrowdSourcingGaugeReportsResponse>> readCrowdSourcingGaugeReports(
            @Path("gaugeId") int gaugeId
    );

    @GET(BuildConfig.REAL_TIME_GAUGE_REPORTS_API_READ_PATH)
    public Observable<List<List<Long>>> readRealTimeGaugeReports(
            @Path("gaugeId") int gaugeId
    );

    @GET(BuildConfig.HISTORIC_GAUGE_REPORTS_API_READ_PATH)
    public Observable<List<List<Long>>> readHistoricGaugeReports(
            @Path("gaugeId") int gaugeId
    );

    @Multipart
    @POST(BuildConfig.CROWD_SOURCING_GAUGE_REPORTS_API_CREATE_PATH)
    public Observable<CreateCrowdSourcingGaugeReportResponse> createCrowdSourcingGaugeReport(
            @Part("gauge") TypedString gauge,
            @Part("water_level") TypedString waterLevel,
            @Part("user_name") TypedString userName,
            @Part("email_address") TypedString emailAddress,
            @Part("recorded_at") TypedString recordedAt,
            @Part("gauge_photo") TypedFile gaugePhoto
    );

}
