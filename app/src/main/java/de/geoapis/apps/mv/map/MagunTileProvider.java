package de.geoapis.apps.mv.map;

import com.google.android.gms.maps.model.UrlTileProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.IllegalFormatException;

public class MagunTileProvider extends UrlTileProvider {

    private static final int TILE_WIDTH = 256;
    private static final int TILE_HEIGHT = 256;
    private int mMinZoom;
    private int mMaxZoom;
    private String mServerBaseUrl;
    private static final String TILE_PARAMETER_PART = "%d/%d/%d.png";

    public MagunTileProvider(final String serverBaseUrl, int minZoom, int maxZoom) {
        this(TILE_WIDTH, TILE_HEIGHT, serverBaseUrl, minZoom, maxZoom);
    }

    public MagunTileProvider(int width, int height,
                             final String serverBaseUrl,
                             int minZoom, int maxZoom) {
        super(width, height);
        mServerBaseUrl = serverBaseUrl;
        mMinZoom = minZoom;
        mMaxZoom = maxZoom;
    }

    public URL getTileUrl(int x, int y, int zoom) {
        // Move coordinate origin from top left to bottom left.
        int numTilesY = 1 << zoom;
        int reversedY = numTilesY - y - 1;

        /* Define the URL pattern for the tile images */
        String tileParameterPart = null;
        try {
            tileParameterPart = String.format(TILE_PARAMETER_PART, zoom, x, reversedY);
        } catch (IllegalFormatException e) {
            e.printStackTrace();
        }
        String tileServerUrl = mServerBaseUrl + tileParameterPart;

        if (!checkTileExists(x, y, zoom)) {
            return null;
        }

        try {
            return new URL(tileServerUrl);
        } catch (MalformedURLException exception) {
            throw new AssertionError(exception);
        }
    }

    /*
     * Check that the tile server supports the requested x, y and zoom.
     * Complete this stub according to the tile range you support.
     * If you support a limited range of tiles at different zoom levels, then you
     * need to define the supported x, y range at each zoom level.
     */
    private boolean checkTileExists(int x, int y, int zoom) {
        if ((zoom < mMinZoom || zoom > mMaxZoom)) {
            return false;
        }
        return true;
    }

}
