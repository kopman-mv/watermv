package de.geoapis.apps.mv.data.model.gauges;

import android.content.Context;
import com.fasterxml.jackson.annotation.*;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.utils.StringHelper;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "ezg_fl_d",
        "see_gn",
        "drainage_area",
        "gwk_gn",
        "easting",
        "northing",
        "water_key_lawa",
        "area_key_lawa",
        "pg_code",
        "ezg_fl_dp",
        "water_type_gauge_code",
        "pg_nr",
        "gauge_name",
        "gauge_datum",
        "gauge_datum_height_system",
        "time_series_start",
        "time_series_range",
        "nw",
        "mnw",
        "mw",
        "mhw",
        "hw",
        "nnw",
        "hhw",
        "nq",
        "mnq",
        "mq",
        "mhq",
        "hq",
        "nnq",
        "hhq",
})
public class Properties {

    @JsonProperty("pg_code")
    private String gaugeCode;
    @JsonProperty("pg_nr")
    private String gaugeNumberWsa;
    @JsonProperty("gauge_name")
    private String gaugeName;
    @JsonProperty("area_key_lawa")
    private String areaKeyLawa;
    @JsonProperty("water_key_lawa")
    private String waterKeyLawa;
    @JsonProperty("gwk_gn")
    private String waterNameLawaRoute;
    @JsonProperty("see_gn")
    private String lakeName;
    @JsonProperty("water_type_gauge_code")
    private String waterTypeGauge;
    @JsonProperty("easting")
    private String easting;
    @JsonProperty("northing")
    private String northing;
    @JsonProperty("drainage_area")
    private String drainageArea;
    @JsonProperty("ezg_fl_d")
    private String directDrainageArea;
    @JsonProperty("ezg_fl_dp")
    private String directDrainageAreaPercentual;
    @JsonProperty("gauge_datum")
    private String gaugeDatum;
    @JsonProperty("gauge_datum_height_system")
    private String gaugeDatumHeightSystem;
    @JsonProperty("time_series_start")
    private String timeSeriesStart;
    @JsonProperty("time_series_range")
    private String timeSeriesRange;

    // Pegelhauptwerte

    @JsonProperty("nw")
    private String principalValueNw;
    @JsonProperty("mnw")
    private String principalValueMnw;
    @JsonProperty("mw")
    private String principalValueMw;
    @JsonProperty("mhw")
    private String principalValueMhw;
    @JsonProperty("hw")
    private String principalValueHw;
    @JsonProperty("nnw")
    private String principalValueNnw;
    @JsonProperty("hhw")
    private String principalValueHhw;
    @JsonProperty("nq")
    private String principalValueNq;
    @JsonProperty("mnq")
    private String principalValueMnq;
    @JsonProperty("mq")
    private String principalValueMq;
    @JsonProperty("mhq")
    private String principalValueMhq;
    @JsonProperty("hq")
    private String principalValueHq;
    @JsonProperty("nnq")
    private String principalValueNnq;
    @JsonProperty("hhq")
    private String principalValueHhq;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The directDrainageArea
     */
    @JsonProperty("ezg_fl_d")
    public String getDirectDrainageArea() {
        return directDrainageArea;
    }

    /**
     * @param directDrainageArea The ezg_fl_d
     */
    @JsonProperty("ezg_fl_d")
    public void setDirectDrainageArea(String directDrainageArea) {
        this.directDrainageArea = directDrainageArea;
    }

    /**
     * @return The lakeName
     */
    @JsonProperty("see_gn")
    public String getLakeName() {
        return lakeName;
    }

    /**
     * @param lakeName The see_gn
     */
    @JsonProperty("see_gn")
    public void setLakeName(String lakeName) {
        this.lakeName = lakeName;
    }

    protected static final String DRAINAGE_AREA_DEFAULT = "0.0";

    /**
     * @return The drainageArea
     */
    @JsonProperty("drainage_area")
    public String getDrainageArea() {
        return StringHelper.getValueOrNull(drainageArea, DRAINAGE_AREA_DEFAULT);
    }

    /**
     * @param drainageArea drainage_area
     */
    @JsonProperty("drainage_area")
    public void setDrainageArea(String drainageArea) {
        this.drainageArea = drainageArea;
    }

    /**
     * @return The waterNameLawaRoute
     */
    @JsonProperty("gwk_gn")
    public String getWaterNameLawaRoute() {
        return waterNameLawaRoute;
    }

    /**
     * @param waterNameLawaRoute The gwk_gn
     */
    @JsonProperty("gwk_gn")
    public void setWaterNameLawaRoute(String waterNameLawaRoute) {
        this.waterNameLawaRoute = waterNameLawaRoute;
    }

    /**
     * @return The easting
     */
    @JsonProperty("easting")
    public String getEasting() {
        return easting;
    }

    /**
     * @param easting The easting
     */
    @JsonProperty("easting")
    public void setEasting(String easting) {
        this.easting = easting;
    }

    /**
     * @return The northing
     */
    @JsonProperty("northing")
    public String getNorthing() {
        return northing;
    }

    /**
     * @param northing The northing
     */
    @JsonProperty("northing")
    public void setNorthing(String northing) {
        this.northing = northing;
    }

    protected static final String WATER_KEY_LAWA_DEFAULT = "0.0";

    /**
     * @return The waterKeyLawa
     */
    @JsonProperty("water_key_lawa")
    public String getWaterKeyLawa() {
        return StringHelper.getValueOrNull(waterKeyLawa, WATER_KEY_LAWA_DEFAULT);
    }

    /**
     * @param waterKeyLawa The water_key_lawa
     */
    @JsonProperty("water_key_lawa")
    public void setWaterKeyLawa(String waterKeyLawa) {
        this.waterKeyLawa = waterKeyLawa;
    }

    protected static final String AREA_KEY_LAWA_DEFAULT = "0.0";

    /**
     * @return The areaKeyLawa
     */
    @JsonProperty("area_key_lawa")
    public String getAreaKeyLawa() {
        return StringHelper.getValueOrNull(areaKeyLawa, AREA_KEY_LAWA_DEFAULT);
    }

    /**
     * @param areaKeyLawa The area_key_lawa
     */
    @JsonProperty("area_key_lawa")
    public void setAreaKeyLawa(String areaKeyLawa) {
        this.areaKeyLawa = areaKeyLawa;
    }

    /**
     * @return The gaugeCode
     */
    @JsonProperty("pg_code")
    public String getGaugeCode() {
        return gaugeCode;
    }

    /**
     * @param gaugeCode The pg_code
     */
    @JsonProperty("pg_code")
    public void setGaugeCode(String gaugeCode) {
        this.gaugeCode = gaugeCode;
    }

    /**
     * @return The directDrainageAreaPercentual
     */
    @JsonProperty("ezg_fl_dp")
    public String getDirectDrainageAreaPercentual() {
        return directDrainageAreaPercentual;
    }

    /**
     * @param directDrainageAreaPercentual The ezg_fl_dp
     */
    @JsonProperty("ezg_fl_dp")
    public void setDirectDrainageAreaPercentual(String directDrainageAreaPercentual) {
        this.directDrainageAreaPercentual = directDrainageAreaPercentual;
    }

    /**
     * @return The waterTypeGauge
     */
    @JsonProperty("water_type_gauge_code")
    public String getWaterTypeGauge() {
        return waterTypeGauge;
    }

    protected static final String WATER_TYPE_GAUGE_STREAM = "f";

    protected static final String WATER_TYPE_GAUGE_LAKE = "s";

    protected static final String WATER_TYPE_GAUGE_LAKE_DOMINATED = "s_";

    protected static final String WATER_TYPE_GAUGE_CODE_STREAM = "201";

    protected static final String WATER_TYPE_GAUGE_CODE_STANDING_WATER = "202";

    protected static final String WATER_TYPE_GAUGE_CODE_COASTAL_WATER = "203";

    protected static final String WATER_TYPE_GAUGE_CODE_UNKNOWN = "204";

    protected static final String WATER_TYPE_GAUGE_CODE_OUTER_COAST = "210";

    protected static final String WATER_TYPE_GAUGE_CODE_INNER_COAST = "211";

    protected static final String WATER_TYPE_GAUGE_CODE_DEFAULT = "0.0";

    // Convert type abbreviation into its meaning, see dlm25w_pegel.pdf
    // TODO: A key-value pair would be nice to manage the
    // abbreviations and their long forms in one place.
    public String getWaterTypeGauge(final Context context) {
        if (WATER_TYPE_GAUGE_STREAM.equals(waterTypeGauge)) {
            return context.getString(R.string.value_water_type_gauge_stream);
        }
        if (WATER_TYPE_GAUGE_LAKE.equals(waterTypeGauge)) {
            return context.getString(R.string.value_water_type_gauge_lake);
        }
        if (WATER_TYPE_GAUGE_LAKE_DOMINATED.equals(waterTypeGauge)) {
            return context.getString(R.string.value_water_type_gauge_stream_lake_dominated);
        }
        String sanitizedWaterTypeGauge =
                StringHelper.getIntegerValueAsStringOrNull(waterTypeGauge);

        if (WATER_TYPE_GAUGE_CODE_STREAM.equals(sanitizedWaterTypeGauge)) {
            return context.getString(R.string.water_type_gauge_code_stream);
        }
        if (WATER_TYPE_GAUGE_CODE_STANDING_WATER.equals(sanitizedWaterTypeGauge)) {
            return context.getString(R.string.water_type_gauge_code_standing_water);
        }
        if (WATER_TYPE_GAUGE_CODE_COASTAL_WATER.equals(sanitizedWaterTypeGauge)) {
            return context.getString(R.string.water_type_gauge_code_coastal_water);
        }
        if (WATER_TYPE_GAUGE_CODE_UNKNOWN.equals(sanitizedWaterTypeGauge)) {
            return context.getString(R.string.water_type_gauge_code_unknown);
        }
        if (WATER_TYPE_GAUGE_CODE_OUTER_COAST.equals(sanitizedWaterTypeGauge)) {
            return context.getString(R.string.water_type_gauge_code_outer_coast);
        }
        if (WATER_TYPE_GAUGE_CODE_INNER_COAST.equals(sanitizedWaterTypeGauge)) {
            return context.getString(R.string.water_type_gauge_code_inner_coast);
        }
        return StringHelper.getValueOrNull(waterTypeGauge, WATER_TYPE_GAUGE_CODE_DEFAULT);
    }

    /**
     * @param waterTypeGauge The water_type_gauge_code
     */
    @JsonProperty("water_type_gauge_code")
    public void setWaterTypeGauge(String waterTypeGauge) {
        this.waterTypeGauge = waterTypeGauge;
    }

    /**
     * @return The gaugeNumberWsa
     */
    @JsonProperty("pg_nr")
    public String getGaugeNumberWsa() {
        return gaugeNumberWsa;
    }

    /**
     * @param gaugeNumberWsa The pg_nr
     */
    @JsonProperty("pg_nr")
    public void setGaugeNumberWsa(String gaugeNumberWsa) {
        this.gaugeNumberWsa = gaugeNumberWsa;
    }

    /**
     * @return The gaugeName
     */
    @JsonProperty("gauge_name")
    public String getGaugeName() {
        return gaugeName;
    }

    /**
     * @param gaugeName The gaugeName
     */
    @JsonProperty("gauge_name")
    public void setGaugeName(String gaugeName) {
        this.gaugeName = gaugeName;
    }

    @JsonProperty("gauge_datum")
    public String getGaugeDatum() {
        return gaugeDatum;
    }

    @JsonProperty("gauge_datum")
    public void setGaugeDatum(String gaugeDatum) {
        this.gaugeDatum = gaugeDatum;
    }

    @JsonProperty("gauge_datum_height_system")
    public String getGaugeDatumHeightSystem() {
        return gaugeDatumHeightSystem;
    }

    @JsonProperty("gauge_datum_height_system")
    public void setGaugeDatumHeightSystem(String gaugeDatumHeightSystem) {
        this.gaugeDatumHeightSystem = gaugeDatumHeightSystem;
    }

    @JsonProperty("time_series_range")
    public String getTimeSeriesRange() {
        return timeSeriesRange;
    }

    @JsonProperty("time_series_range")
    public void setTimeSeriesRange(String timeSeriesRange) {
        this.timeSeriesRange = timeSeriesRange;
    }

    protected static final String TIME_SERIES_START_DEFAULT = "0.0";

    @JsonProperty("time_series_start")
    public String getTimeSeriesStart() {
        String value = StringHelper.getValueOrNull(timeSeriesStart, TIME_SERIES_START_DEFAULT);
        return StringHelper.getIntegerValueAsStringOrNull(value);
    }

    @JsonProperty("time_series_start")
    public void setTimeSeriesStart(String timeSeriesStart) {
        this.timeSeriesStart = timeSeriesStart;
    }

    // Pegelhauptwerte

    protected static final String PRINCIPAL_VALUE_DEFAULT = "-1.0";

    @JsonProperty("hhq")
    public String getPrincipalValueHhq() {
        return StringHelper.getValueOrNull(principalValueHhq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("hhq")
    public void setPrincipalValueHhq(String principalValueHhq) {
        this.principalValueHhq = principalValueHhq;
    }

    @JsonProperty("hq")
    public String getPrincipalValueHq() {
        return StringHelper.getValueOrNull(principalValueHq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("hq")
    public void setPrincipalValueHq(String principalValueHq) {
        this.principalValueHq = principalValueHq;
    }

    @JsonProperty("hhw")
    public String getPrincipalValueHhw() {
        return StringHelper.getValueOrNull(principalValueHhw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("hhw")
    public void setPrincipalValueHhw(String principalValueHhw) {
        this.principalValueHhw = principalValueHhw;
    }

    @JsonProperty("hw")
    public String getPrincipalValueHw() {
        return StringHelper.getValueOrNull(principalValueHw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("hw")
    public void setPrincipalValueHw(String principalValueHw) {
        this.principalValueHw = principalValueHw;
    }

    @JsonProperty("mhq")
    public String getPrincipalValueMhq() {
        return StringHelper.getValueOrNull(principalValueMhq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("mhq")
    public void setPrincipalValueMhq(String principalValueMhq) {
        this.principalValueMhq = principalValueMhq;
    }

    @JsonProperty("mhw")
    public String getPrincipalValueMhw() {
        return StringHelper.getValueOrNull(principalValueMhw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("mhw")
    public void setPrincipalValueMhw(String principalValueMhw) {
        this.principalValueMhw = principalValueMhw;
    }

    @JsonProperty("mnq")
    public String getPrincipalValueMnq() {
        return StringHelper.getValueOrNull(principalValueMnq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("mnq")
    public void setPrincipalValueMnq(String principalValueMnq) {
        this.principalValueMnq = principalValueMnq;
    }

    @JsonProperty("mnw")
    public String getPrincipalValueMnw() {
        return StringHelper.getValueOrNull(principalValueMnw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("mnw")
    public void setPrincipalValueMnw(String principalValueMnw) {
        this.principalValueMnw = principalValueMnw;
    }

    @JsonProperty("mq")
    public String getPrincipalValueMq() {
        return StringHelper.getValueOrNull(principalValueMq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("mq")
    public void setPrincipalValueMq(String principalValueMq) {
        this.principalValueMq = principalValueMq;
    }

    @JsonProperty("mw")
    public String getPrincipalValueMw() {
        return StringHelper.getValueOrNull(principalValueMw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("mw")
    public void setPrincipalValueMw(String principalValueMw) {
        this.principalValueMw = principalValueMw;
    }

    @JsonProperty("nnq")
    public String getPrincipalValueNnq() {
        return StringHelper.getValueOrNull(principalValueNnq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("nnq")
    public void setPrincipalValueNnq(String principalValueNnq) {
        this.principalValueNnq = principalValueNnq;
    }

    @JsonProperty("nnw")
    public String getPrincipalValueNnw() {
        return StringHelper.getValueOrNull(principalValueNnw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("nnw")
    public void setPrincipalValueNnw(String principalValueNnw) {
        this.principalValueNnw = principalValueNnw;
    }

    @JsonProperty("nq")
    public String getPrincipalValueNq() {
        return StringHelper.getValueOrNull(principalValueNq, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("nq")
    public void setPrincipalValueNq(String principalValueNq) {
        this.principalValueNq = principalValueNq;
    }

    @JsonProperty("nw")
    public String getPrincipalValueNw() {
        return StringHelper.getValueOrNull(principalValueNw, PRINCIPAL_VALUE_DEFAULT);
    }

    @JsonProperty("nw")
    public void setPrincipalValueNw(String principalValueNw) {
        this.principalValueNw = principalValueNw;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "gaugeName = " + gaugeName;
    }

}
