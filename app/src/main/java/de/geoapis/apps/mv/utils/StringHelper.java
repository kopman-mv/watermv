package de.geoapis.apps.mv.utils;

import android.util.Log;

public class StringHelper {

    public static String getValueOrNull(final String value, final String defaultValue) {
        return defaultValue.equals(value) ? null : value;
    }

    public static String getIntegerValueAsStringOrNull(final String stringValue) {
        if (stringValue == null) {
            return null;
        }
        try {
            return "" + (int) Double.parseDouble(stringValue);
        } catch (Exception e) {
            // Do nothing
            Log.e(StringHelper.class.getName(), e.getMessage());
        }
        return stringValue;
    }

}
