package de.geoapis.apps.mv.data.model;

import com.fasterxml.jackson.annotation.*;
import de.geoapis.apps.mv.BuildConfig;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "url",
        "feature_id",
        "water_level",
        "user_name",
        "email_address",
        "recorded_at",
        "gauge_photo",
        "created_at",
        "modified_at"
})
public class ReadCrowdSourcingGaugeReportsResponse implements
        ReadGaugeReportsResponse {

    @JsonProperty("url")
    private String url;
    @JsonProperty("gauge")
    private String gauge;
    @JsonProperty("water_level")
    private int waterLevel;
    @JsonProperty("user_name")
    private String userName;
    @JsonProperty("email_address")
    private String emailAddress;
    @JsonProperty("recorded_at")
    private String recordedAt;
    @JsonProperty("gauge_photo")
    private String gaugePhoto;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("modified_at")
    private String modifiedAt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The gauge URL
     */
    @JsonProperty("gauge")
    public String getGauge() {
        return gauge;
    }

    public int getGaugeId() {
        // Slice gauge id from path
        String gaugesApiReadPath = BuildConfig.GAUGES_API_READ_PATH;
        int gaugesApiReadPathIndex = gauge.indexOf(gaugesApiReadPath);
        int gaugeIdIndex = gaugesApiReadPathIndex + gaugesApiReadPath.length();
        String gaugeId = gauge.substring(gaugeIdIndex, gauge.length() - 1);
        return Integer.valueOf(gaugeId);
    }

    /**
     * @param gauge The gauge URL
     */
    @JsonProperty("gauge")
    public void setGauge(String gauge) {
        this.gauge = gauge;
    }

    /**
     * @return The waterLevel
     */
    @JsonProperty("water_level")
    public int getWaterLevel() {
        return waterLevel;
    }

    /**
     * @param waterLevel The water_level
     */
    @JsonProperty("water_level")
    public void setWaterLevel(int waterLevel) {
        this.waterLevel = waterLevel;
    }

    /**
     * @return The userName
     */
    @JsonProperty("user_name")
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName The user_name
     */
    @JsonProperty("user_name")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return The emailAddress
     */
    @JsonProperty("email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress The email_address
     */
    @JsonProperty("email_address")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return The gaugePhoto
     */
    @JsonProperty("gauge_photo")
    public String getGaugePhoto() {
        return gaugePhoto;
    }

    /**
     * @param recordedAt The gauge_photo
     */
    @JsonProperty("gauge_photo")
    public void setGaugePhoto(String gaugePhoto) {
        this.gaugePhoto = gaugePhoto;
    }

    /**
     * @return The recordedAt
     */
    @JsonProperty("recorded_at")
    public String getRecordedAt() {
        return recordedAt;
    }

    @Override
    public DateTime getRecordedAtAsDateTime() {
        return new DateTime(recordedAt, DateTimeZone.UTC);
    }

    /**
     * @param recordedAt The recorded_at
     */
    @JsonProperty("recorded_at")
    public void setRecordedAt(String recordedAt) {
        this.recordedAt = recordedAt;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The modifiedAt
     */
    @JsonProperty("modified_at")
    public String getModifiedAt() {
        return modifiedAt;
    }

    /**
     * @param modifiedAt The modified_at
     */
    @JsonProperty("modified_at")
    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
