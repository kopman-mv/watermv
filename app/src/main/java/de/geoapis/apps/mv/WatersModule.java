package de.geoapis.apps.mv;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import dagger.Module;
import dagger.Provides;
import de.geoapis.apps.mv.data.DataModule;
import de.geoapis.apps.mv.map.MapsActivity;
import de.geoapis.apps.mv.prefs.PreferencesHelper;
import de.geoapis.apps.mv.reports.NewReportFragment;

import javax.inject.Singleton;

@Module(
        includes = {
                DataModule.class
        },
        injects = {
                MapsActivity.class,
                NewReportFragment.class,
                PreferencesHelper.class}
)
public final class WatersModule {

    private final Context mContext;

    public WatersModule(final Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferenceHelper() {
        return new PreferencesHelper();
    }

}
