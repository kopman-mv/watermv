package de.geoapis.apps.mv.data.api;

public enum FederalState {
    MV("mv");

    public final String value;

    FederalState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

}
