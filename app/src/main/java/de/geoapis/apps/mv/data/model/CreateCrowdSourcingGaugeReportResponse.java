package de.geoapis.apps.mv.data.model;

public class CreateCrowdSourcingGaugeReportResponse {

    public String url;
    public String gauge;
    public String waterLevel;
    public String userName;
    public String emailAddress;
    public String recordedAt;
    public String gaugePhoto;
    public String createdAt;
    public String modifiedAt;

    @Override
    public String toString() {
        return "CreateCrowdSourcingGaugeReportResponse{" +
                "url='" + url + '\'' +
                ", gauge=" + gauge +
                ", waterLevel='" + waterLevel + '\'' +
                ", userName='" + userName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", recordedAt='" + recordedAt + '\'' +
                ", gaugePhoto='" + gaugePhoto + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", modifiedAt='" + modifiedAt + '\'' +
                '}';
    }

}
