package de.geoapis.apps.mv.data.model.streams;

import android.content.Context;
import com.fasterxml.jackson.annotation.*;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.utils.StringHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.parceler.Parcel;
import org.parceler.Transient;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@Parcel
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "gbk_lawa",
        "gwk_lawa",
        "gn1",
        "gn2",
        "gn3",
        "laenge",
        "land",
        "objart",
        "ofl",
        "wbv",
        "wdm",
        "wrrl"
})
public class Properties {

    @JsonProperty("gbk_lawa")
    public String areaKeyLawa;
    @JsonProperty("gwk_lawa")
    public String waterKeyLawa; // LAWA = Bund/Länder-Arbeitsgemeinschaft Wasser
    @JsonProperty("gn1")
    public String topographicStreamName1;
    @JsonProperty("gn2")
    public String topographicStreamName2;
    @JsonProperty("gn3")
    public String waterwayName;
    @JsonProperty("laenge")
    public String segmentLength;
    @JsonProperty("land")
    public String country;
    @JsonProperty("objart")
    public String objectType;
    @JsonProperty("ofl")
    public String surfaceLocation;
    @JsonProperty("wbv")
    public String wbvNumber; // WBV = Wasser- und Bodenverband
    @JsonProperty("wdm")
    public String waterCategory;
    @JsonProperty("wrrl")
    public String wrrlReportableStream; // WRRL = Wasserrahmenrichtlinie

    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Properties() {
        // Required by Parceler
    }

    /**
     * @return The areaKeyLawa
     */
    @JsonProperty("gbk_lawa")
    public String getAreaKeyLawa() {
        return areaKeyLawa;
    }

    /**
     * @param areaKeyLawa The gbk_lawa
     */
    @JsonProperty("gbk_lawa")
    public void setAreaKeyLawa(String areaKeyLawa) {
        this.areaKeyLawa = areaKeyLawa;
    }

    protected static final String WATER_KEY_LAWA_DEFAULT = "0";

    /**
     * @return The waterKeyLawa
     */
    @JsonProperty("gwk_lawa")
    public String getWaterKeyLawa() {
        return StringHelper.getValueOrNull(waterKeyLawa, WATER_KEY_LAWA_DEFAULT);
    }

    /**
     * @param waterKeyLawa The gwk_lawa
     */
    @JsonProperty("gwk_lawa")
    public void setWaterKeyLawa(String waterKeyLawa) {
        this.waterKeyLawa = waterKeyLawa;
    }

    /**
     * @return The topographicStreamName1
     */
    @JsonProperty("gn1")
    public String getTopographicStreamName1() {
        return topographicStreamName1;
    }

    /**
     * @param topographicStreamName1 The gn1
     */
    @JsonProperty("gn1")
    public void setTopographicStreamName1(String topographicStreamName1) {
        this.topographicStreamName1 = topographicStreamName1;
    }

    /**
     * @return The topographicStreamName2
     */
    @JsonProperty("gn2")
    public String getTopographicStreamName2() {
        return topographicStreamName2;
    }

    /**
     * @param topographicStreamName2 The gn2
     */
    @JsonProperty("gn2")
    public void setTopographicStreamName2(String topographicStreamName2) {
        this.topographicStreamName2 = topographicStreamName2;
    }

    /**
     * @return The waterwayName
     */
    @JsonProperty("gn3")
    public String getWaterwayName() {
        return waterwayName;
    }

    /**
     * @param waterwayName The gn3
     */
    @JsonProperty("gn3")
    public void setWaterwayName(String waterwayName) {
        this.waterwayName = waterwayName;
    }

    /**
     * @return The segmentLength
     */
    @JsonProperty("laenge")
    public String getSegmentLength() {
        return segmentLength;
    }

    /**
     * @param laenge The laenge
     */
    @JsonProperty("laenge")
    public void setSegmentLength(String segmentLength) {
        this.segmentLength = segmentLength;
    }

    /**
     * @return The country
     */
    @JsonProperty("land")
    public String getCountry() {
        if (country == null) {
            return null;
        }
        if (country.equals("O_mv")) {
            return "Ostsee / Mecklenburg-Vorpommern";
        }
        if (country.equals("bb")) {
            return "Brandenburg";
        }
        if (country.equals("bbni")) {
            return "Grenze Brandenburg / Niedersachsen";
        }
        if (country.equals("bbpl")) {
            return "Grenze Brandenburg / Polen";
        }
        if (country.equals("bbst")) {
            return "Grenze Brandenburg / Sachsen-Anhalt";
        }
        if (country.equals("be")) {
            return "Berlin";
        }
        if (country.equals("hh")) {
            return "Hamburg";
        }
        if (country.equals("hhni")) {
            return "Grenze Hamburg / Niedersachsen";
        }
        if (country.equals("hhsh")) {
            return "Grenze Hamburg / Schleswig-Holstein";
        }
        if (country.equals("mv")) {
            return "Mecklenburg-Vorpommern";
        }
        if (country.equals("mvbb")) {
            return "Grenze Mecklenburg-Vorpommern / Brandenburg";
        }
        if (country.equals("mvni")) {
            return "Grenze Mecklenburg-Vorpommern / Niedersachsen";
        }
        if (country.equals("mvpl")) {
            return "Grenze Mecklenburg-Vorpommern / Polen";
        }
        if (country.equals("mvsh")) {
            return "Grenze Mecklenburg-Vorpommern / Schleswig-Holstein";
        }
        if (country.equals("ni")) {
            return "Niedersachsen";
        }
        if (country.equals("nish")) {
            return "Grenze Niedersachsen / Schleswig-Holstein";
        }
        if (country.equals("nist")) {
            return "Grenze Niedersachsen / Sachsen-Anhalt";
        }
        if (country.equals("pl")) {
            return "Polen";
        }
        if (country.equals("sh")) {
            return "Schleswig-Holstein";
        }
        if (country.equals("st")) {
            return "Sachsen-Anhalt";
        }
        return country;
    }

    /**
     * @param country The land
     */
    @JsonProperty("land")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The objectType
     */
    @JsonProperty("objart")
    public String getObjectType() {
        return objectType;
    }

    public String getObjectType(Context context) {
        if (objectType == null) {
            return null;
        }
        if (objectType.equals("5101")) {
            return context.getString(R.string.value_object_type_river_brook);
        }
        if (objectType.equals("5102")) {
            return context.getString(R.string.value_object_type_channel);
        }
        if (objectType.equals("5103")) {
            return context.getString(R.string.value_object_type_ditch);
        }
        return objectType;
    }

    /**
     * @param objectType The objart
     */
    @JsonProperty("objart")
    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    /**
     * @return The surfaceLocation
     */
    @JsonProperty("ofl")
    public String getSurfaceLocation() {
        return surfaceLocation;
    }

    public String getSurfaceLocation(Context context) {
        if (surfaceLocation == null) {
            return null;
        }
        if (surfaceLocation.equals("1100")) {
            return context.getString(R.string.value_surface_location_aboveground);
        }
        if (surfaceLocation.equals("1800")) {
            return context.getString(R.string.value_surface_location_pipeline);
        }
        if (surfaceLocation.equals("2000")) {
            return context.getString(R.string.value_surface_location_passage);
        }
        if (surfaceLocation.equals("3000")) {
            return context.getString(R.string.value_surface_location_culvert);
        }
        if (surfaceLocation.equals("4000")) {
            return context.getString(R.string.value_surface_location_seep_way);
        }
        return surfaceLocation;
    }

    /**
     * @param surfaceLocation The ofl
     */
    @JsonProperty("ofl")
    public void setSurfaceLocation(String surfaceLocation) {
        this.surfaceLocation = surfaceLocation;
    }

    protected static final String WBV_DEFAULT = "0";

    /**
     * @return The wbvNumber
     */
    @JsonProperty("wbv")
    public String getWbvNumber() {
        return wbvNumber;
    }

    public String getWbvName() {
        if (wbvNumber == null) {
            return null;
        }
        if (wbvNumber.equals("1")) {
            return "Boize-Sude-Schaale";
        }
        if (wbvNumber.equals("2")) {
            return "Untere Elde";
        }
        if (wbvNumber.equals("4")) {
            return "Stepenitz / Maurine";
        }
        if (wbvNumber.equals("5")) {
            return "Schweriner See / Obere Sude";
        }
        if (wbvNumber.equals("6")) {
            return "Obere Warnow";
        }
        if (wbvNumber.equals("7")) {
            return "Mittlere Elde";
        }
        if (wbvNumber.equals("8")) {
            return "Mildenitz / Lübzer Elde";
        }
        if (wbvNumber.equals("9")) {
            return "Nebel";
        }
        if (wbvNumber.equals("10")) {
            return "Warnow - Beke";
        }
        if (wbvNumber.equals("11")) {
            return "Wallensteingraben - Küste";
        }
        if (wbvNumber.equals("12")) {
            return "Hellbach - Conventer Niederung";
        }
        if (wbvNumber.equals("13")) {
            return "Untere Warnow - Küste";
        }
        if (wbvNumber.equals("14")) {
            return "Recknitz - Boddenkette";
        }
        if (wbvNumber.equals("15")) {
            return "Trebel";
        }
        if (wbvNumber.equals("16")) {
            return "Barthe - Küste";
        }
        if (wbvNumber.equals("17")) {
            return "Rügen";
        }
        if (wbvNumber.equals("18")) {
            return "Ryck - Ziese";
        }
        if (wbvNumber.equals("19")) {
            return "Insel Usedom - Peenestrom";
        }
        if (wbvNumber.equals("20")) {
            return "Müritz";
        }
        if (wbvNumber.equals("22")) {
            return "Obere Peene";
        }
        if (wbvNumber.equals("23")) {
            return "Teterower Peene";
        }
        if (wbvNumber.equals("24")) {
            return "Obere Havel / Obere Tollense";
        }
        if (wbvNumber.equals("25")) {
            return "Untere Tollense - Mittlere Peene";
        }
        if (wbvNumber.equals("27")) {
            return "Untere Peene";
        }
        if (wbvNumber.equals("28")) {
            return "Landgraben";
        }
        if (wbvNumber.equals("30")) {
            return "Uecker - Haffküste";
        }
        if (wbvNumber.equals("31")) {
            return "Mittlere Uecker - Randow";
        }
        return StringHelper.getValueOrNull(wbvNumber, WBV_DEFAULT);
    }

    /**
     * @param wbvNumber The wbv
     */
    @JsonProperty("wbv")
    public void setWbvNumber(String wbvNumber) {
        this.wbvNumber = wbvNumber;
    }

    /**
     * @return The waterCategory
     */
    @JsonProperty("wdm")
    public String getWaterCategory() {
        return waterCategory;
    }

    public String getWaterCategory(Context context) {
        if (waterCategory == null) {
            return null;
        }
        if (waterCategory.equals("1501")) {
            return context.getString(R.string.value_water_category_1);
        }
        if (waterCategory.equals("1502")) {
            return context.getString(R.string.value_water_category_2);
        }
        if (waterCategory.equals("1503")) {
            return context.getString(R.string.value_water_category_3);
        }
        if (waterCategory.equals("1504")) {
            return context.getString(R.string.value_water_category_4);
        }
        return waterCategory;
    }

    /**
     * @param waterCategory The wdm
     */
    @JsonProperty("wdm")
    public void setWaterCategory(String waterCategory) {
        this.waterCategory = waterCategory;
    }

    /**
     * @return The wrrlReportableStream
     */
    @JsonProperty("wrrl")
    public String getWrrlReportableStream() {
        return wrrlReportableStream;
    }

    public String getWrrlReportableStream(Context context) {
        if (wrrlReportableStream == null) {
            return null;
        }
        if (wrrlReportableStream.equals("0")) {
            return context.getString(R.string.value_wrrl_reportable_stream_not);
        }
        if (wrrlReportableStream.equals("1")) {
            return context.getString(R.string.value_wrrl_reportable_stream_yes);
        }
        return wrrlReportableStream;
    }

    /**
     * @param wrrlReportableStream The wrrl
     */
    @JsonProperty("wrrl")
    public void setWrrlReportableStream(String wrrlReportableStream) {
        this.wrrlReportableStream = wrrlReportableStream;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
