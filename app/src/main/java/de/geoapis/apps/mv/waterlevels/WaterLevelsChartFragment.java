package de.geoapis.apps.mv.waterlevels;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.view.*;
import android.widget.CheckBox;
import butterknife.InjectView;
import butterknife.OnClick;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.data.model.HistoricGaugeReport;
import de.geoapis.apps.mv.data.model.ReadCrowdSourcingGaugeReportsResponse;
import de.geoapis.apps.mv.data.model.ReadGaugeReportsResponse;
import de.geoapis.apps.mv.data.model.RealTimeGaugeReport;
import de.geoapis.apps.mv.utils.DateTimeFormatter;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.*;
import lecho.lib.hellocharts.view.LineChartView;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WaterLevelsChartFragment extends WaterLevelsFragment {

    public static final String FRAGMENT_TAG =
            BuildConfig.APPLICATION_ID + ".WATER_LEVELS_CHART_FRAGMENT_TAG";

    @InjectView(R.id.water_levels_chart_container)
    View mWaterLevelsChartContainer;

    @InjectView(R.id.water_levels_chart)
    LineChartView mWaterLevelsChartView;

    @InjectView(R.id.water_levels_chart_crowd_sourcing_reports_legend)
    CheckBox mWaterLevelsChartCrowdSourcingCheckBox;

    @InjectView(R.id.water_levels_chart_real_time_reports_legend)
    CheckBox mWaterLevelsChartRealTimeReportsCheckBox;

    @InjectView(R.id.water_levels_chart_historic_reports_legend)
    CheckBox mWaterLevelsChartHistoricReportsCheckBox;

    @InjectView(R.id.water_levels_chart_empty)
    View mWaterLevelsChartEmptyView;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_water_levels_chart, container, false);
        inject(rootView);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.reports_chart, menu);
    }

    @Override
    protected void updateView() {
        if ((mCrowdSourcingGaugeReports == null || mCrowdSourcingGaugeReports.isEmpty())
                && (mRealTimeGaugeReports == null || mRealTimeGaugeReports.isEmpty())
                && (mHistoricGaugeReports == null || mHistoricGaugeReports.isEmpty())) {
            mWaterLevelsChartContainer.setVisibility(View.GONE);
            mWaterLevelsChartEmptyView.setVisibility(View.VISIBLE);
        } else {
            mWaterLevelsChartContainer.setVisibility(View.VISIBLE);
            mWaterLevelsChartEmptyView.setVisibility(View.GONE);
            renderChart();
        }

    }

    private void renderChart() {
        List<ReadGaugeReportsResponse> chronologicalReports = new ArrayList<>();
        // Crowd sourcing gauge reports
        if (mCrowdSourcingGaugeReports == null || mCrowdSourcingGaugeReports.isEmpty()) {
            mWaterLevelsChartCrowdSourcingCheckBox.setEnabled(false);
        } else {
            mWaterLevelsChartCrowdSourcingCheckBox.setEnabled(true);
            if (mWaterLevelsChartCrowdSourcingCheckBox.isChecked()) {
                chronologicalReports.addAll(mCrowdSourcingGaugeReports);
            }
        }
        // Real time gauge reports
        if (mRealTimeGaugeReports == null || mRealTimeGaugeReports.isEmpty()) {
            mWaterLevelsChartRealTimeReportsCheckBox.setEnabled(false);
        } else {
            mWaterLevelsChartRealTimeReportsCheckBox.setEnabled(true);
            if (mWaterLevelsChartRealTimeReportsCheckBox.isChecked()) {
                chronologicalReports.addAll(mRealTimeGaugeReports);
            }
        }
        // Historic gauge reports
        if (mHistoricGaugeReports == null || mHistoricGaugeReports.isEmpty()) {
            mWaterLevelsChartHistoricReportsCheckBox.setEnabled(false);
        } else {
            mWaterLevelsChartHistoricReportsCheckBox.setEnabled(true);
            if (mWaterLevelsChartHistoricReportsCheckBox.isChecked()) {
                chronologicalReports.addAll(mHistoricGaugeReports);
            }
        }

        Collections.sort(chronologicalReports, (lhs, rhs) ->
                lhs.getRecordedAtAsDateTime().compareTo(rhs.getRecordedAtAsDateTime()));

        List<PointValue> crowdSourcingWaterLevelValues = new ArrayList<PointValue>();
        List<PointValue> realTimeWaterLevelValues = new ArrayList<PointValue>();
        List<PointValue> historicWaterLevelValues = new ArrayList<PointValue>();
        List<AxisValue> axisValues = new ArrayList<AxisValue>();

        int reportsCount = chronologicalReports.size();
        for (int x = 0; x < reportsCount; ++x) {

            int endToStartIndex = reportsCount - 1 - x;
            ReadGaugeReportsResponse response = chronologicalReports.get(endToStartIndex);

            DateTime recordedAt = response.getRecordedAtAsDateTime();
            long recordedAtInMilliseconds = recordedAt.getMillis();
            int waterLevel = response.getWaterLevel();
            PointValue dataPoint = new PointValue(recordedAtInMilliseconds, waterLevel);
            if (response instanceof ReadCrowdSourcingGaugeReportsResponse) {
                crowdSourcingWaterLevelValues.add(dataPoint);
            } else if (response instanceof RealTimeGaugeReport) {
                realTimeWaterLevelValues.add(dataPoint);
            } else if (response instanceof HistoricGaugeReport) {
                historicWaterLevelValues.add(dataPoint);
            } else {
                throw new IllegalStateException("Unknown response type: " + response);
            }

            AxisValue axisValue = new AxisValue(recordedAtInMilliseconds);
            String formattedRecordedAt = DateTimeFormatter.getFormattedDateTime(
                    recordedAt, getString(R.string.date_time_display_pattern));
            axisValue.setLabel(formattedRecordedAt);
            axisValues.add(axisValue);
        }

        Line crowdSourcingGaugeReportsLine = getWaterLevelLine(
                crowdSourcingWaterLevelValues,
                R.color.water_levels_chart_crowd_sourcing_reports_line,
                mWaterLevelsChartCrowdSourcingCheckBox.isChecked());
        Line realTimeGaugeReportsLine = getWaterLevelLine(
                realTimeWaterLevelValues,
                R.color.water_levels_chart_real_time_reports_line,
                mWaterLevelsChartRealTimeReportsCheckBox.isChecked());
        Line historicGaugeReportsLine = getWaterLevelLine(
                historicWaterLevelValues,
                R.color.water_levels_chart_historic_reports_line,
                mWaterLevelsChartHistoricReportsCheckBox.isChecked());

        List<Line> lines = new ArrayList<Line>();
        lines.add(crowdSourcingGaugeReportsLine);
        lines.add(realTimeGaugeReportsLine);
        lines.add(historicGaugeReportsLine);

        Resources resources = getResources();
        LineChartData lineChartData = new LineChartData();
        lineChartData.setLines(lines);
        lineChartData.setAxisXBottom(new Axis(axisValues)
                        .setName(resources.getString(R.string.water_levels_chart_x_axis_name))
                        .setMaxLabelChars(resources.getInteger(R.integer.water_levels_chart_maximum_label_characters))
                        .setLineColor(resources.getColor(R.color.water_levels_chart_x_axis))
                        .setTextColor(resources.getColor(R.color.water_levels_chart_x_axis_text))
                        .setHasSeparationLine(true)
                        .setHasLines(true)
        );
        lineChartData.setAxisYLeft(new Axis()
                        .setName(resources.getString(R.string.water_levels_chart_y_axis_name))
                        .setHasLines(true)
                        .setLineColor(resources.getColor(R.color.water_levels_chart_y_axis))
                        .setTextColor(resources.getColor(R.color.water_levels_chart_y_axis_text))
                        .setHasTiltedLabels(true)
                        .setHasSeparationLine(true)
        );

        mWaterLevelsChartView.setLineChartData(lineChartData);
        mWaterLevelsChartView.setInteractive(true);
        mWaterLevelsChartView.setZoomType(ZoomType.HORIZONTAL);
        mWaterLevelsChartView.setValueSelectionEnabled(true);
    }

    private Line getWaterLevelLine(List<PointValue> waterLevelValues,
                                   @ColorRes int lineColor,
                                   boolean visible) {
        Resources resources = getResources();
        return new Line(waterLevelValues)
                .setColor(resources.getColor(lineColor))
                .setHasLabelsOnlyForSelected(true)
                .setHasLines(visible)
                .setHasPoints(visible)
                .setStrokeWidth(resources.getInteger(R.integer.water_levels_chart_data_line_stroke_width))
                .setCubic(false);
    }

    @OnClick(R.id.water_levels_chart_crowd_sourcing_reports_legend)
    public void onCrowdSourcingReportsCheckBoxClicked(CheckBox crowdSourcingLegendCheckBox) {
        updateView();
    }

    @OnClick(R.id.water_levels_chart_real_time_reports_legend)
    public void onRealTimeReportsCheckBoxClicked(CheckBox realTimeLegendCheckBox) {
        updateView();
    }

    @OnClick(R.id.water_levels_chart_historic_reports_legend)
    public void onHistoricReportsCheckBoxClicked(CheckBox historicLegendCheckBox) {
        updateView();
    }

}
