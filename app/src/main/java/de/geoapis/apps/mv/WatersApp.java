package de.geoapis.apps.mv;

import android.app.Application;
import com.google.android.gms.maps.model.LatLng;

public class WatersApp extends Application {

    // Initial camera position for the map
    public static final LatLng BOUNDS_NORTH_WEST = new LatLng(
            BuildConfig.BOUNDS_NORTH_WEST_LATITUDE,
            BuildConfig.BOUNDS_NORTH_WEST_LONGITUDE
    );
    public static final LatLng BOUNDS_SOUTH_EAST = new LatLng(
            BuildConfig.BOUNDS_SOUTH_EAST_LATITUDE,
            BuildConfig.BOUNDS_SOUTH_EAST_LONGITUDE
    );

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init(new WatersModule(this));
    }

}
