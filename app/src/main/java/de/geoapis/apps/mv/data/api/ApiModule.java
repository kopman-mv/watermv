package de.geoapis.apps.mv.data.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.reports.NewReportFragment;
import de.geoapis.apps.mv.waterlevels.WaterLevelsChartFragment;
import de.geoapis.apps.mv.waterlevels.WaterLevelsListFragment;
import retrofit.Endpoint;
import retrofit.Endpoints;
import retrofit.RestAdapter;
import retrofit.converter.JacksonConverter;

@Module(
        complete = false,
        library = true,
        injects = {
                NewReportFragment.class,
                WaterLevelsListFragment.class,
                WaterLevelsChartFragment.class
        }
)

public final class ApiModule {

    public static final String GAUGE_FEATURES = "GAUGE_FEATURES";
    public static final String GAUGE_REPORTS = "GAUGE_REPORTS";

    @Provides
    @Singleton
    @Named(GAUGE_FEATURES)
    Endpoint provideGaugeFeaturesEndpoint() {
        return Endpoints.newFixedEndpoint(BuildConfig.GAUGE_FEATURES_API_END_POINT);
    }

    @Provides
    @Singleton
    @Named(GAUGE_REPORTS)
    Endpoint provideGaugeReportsEndpoint() {
        return Endpoints.newFixedEndpoint(BuildConfig.GAUGE_REPORTS_API_END_POINT);
    }

    @Provides
    @Singleton
    ObjectMapper provideObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(
                PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        return objectMapper;
    }

    @Provides
    @Singleton
    @Named(GAUGE_FEATURES)
    GaugeFeaturesService provideGaugeFeaturesService(
            @Named(GAUGE_FEATURES) Endpoint endpoint,
            ObjectMapper objectMapper) {
        return createRestAdapterBuilder(objectMapper)
                .setEndpoint(endpoint)
                .build()
                .create(GaugeFeaturesService.class);
    }

    @Provides
    @Singleton
    @Named(GAUGE_REPORTS)
    GaugeReportsService provideGaugeReportsService(
            @Named(GAUGE_REPORTS) Endpoint endpoint,
            ObjectMapper objectMapper) {
        return createRestAdapterBuilder(objectMapper)
                .setEndpoint(endpoint)
                .build()
                .create(GaugeReportsService.class);
    }

    private RestAdapter.Builder createRestAdapterBuilder(ObjectMapper objectMapper) {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.HEADERS)
                .setConverter(new JacksonConverter(objectMapper));
    }

}
