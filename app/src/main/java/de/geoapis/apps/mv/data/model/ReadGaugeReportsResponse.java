package de.geoapis.apps.mv.data.model;

import org.joda.time.DateTime;

public interface ReadGaugeReportsResponse {

    int getWaterLevel();

    String getUserName();

    String getGaugePhoto();

    String getRecordedAt();

    DateTime getRecordedAtAsDateTime();

}
