package de.geoapis.apps.mv.contract;

import de.geoapis.apps.mv.BuildConfig;

public interface BundleKeys {

    public static final String SELECTED_MARKER =
            BuildConfig.APPLICATION_ID + ".BUNDLE_KEY_SELECTED_MARKER";

    public static final String SELECTED_STREAM =
            BuildConfig.APPLICATION_ID + ".BUNDLE_KEY_SELECTED_STREAM";

    public static final String DATE_TIME_IN_MILLISECONDS =
            BuildConfig.APPLICATION_ID + ".BUNDLE_KEY_DATE_TIME_IN_MILLISECONDS";

}
