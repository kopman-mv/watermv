package de.geoapis.apps.mv.reports;

import android.os.Bundle;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.base.BaseActivity;

public class NewReportActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            addFragment(R.id.new_report_container,
                    new NewReportFragment(),
                    NewReportFragment.FRAGMENT_TAG);
        }
    }

}
