package de.geoapis.apps.mv.waterlevels;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.squareup.picasso.Picasso;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.data.model.ReadGaugeReportsResponse;
import de.geoapis.apps.mv.utils.DateTimeFormatter;

public class WaterLevelViewHolder extends RecyclerView.ViewHolder {

    @InjectView(R.id.gauge_report_water_level)
    TextView waterLevelTextView;

    @InjectView(R.id.gauge_report_reported_by)
    TextView reportedByTextView;

    @InjectView(R.id.gauge_report_recorded_at)
    TextView recordedAtTextView;

    @InjectView(R.id.gauge_report_gauge_photo_thumbnail)
    ImageView gaugePhotoThumbnailImageView;

    private View rootView;

    public WaterLevelViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
        rootView = itemView;
    }

    public void updateContent(Context context, ReadGaugeReportsResponse gaugeReport) {
        String dateTimePattern = context.getString(R.string.date_time_display_pattern);
        waterLevelTextView.setText("" + gaugeReport.getWaterLevel() + " cm");
        reportedByTextView.setText("" + gaugeReport.getUserName());
        recordedAtTextView.setText("" + DateTimeFormatter.getFormattedDateTime(
                gaugeReport.getRecordedAtAsDateTime(), dateTimePattern));
        Picasso.with(context)
                .load(gaugeReport.getGaugePhoto())
                .resizeDimen(R.dimen.water_levels_list_gauge_photo_thumbnail_dimension, R.dimen.water_levels_list_gauge_photo_thumbnail_dimension)
                .centerCrop()
                .placeholder(R.drawable.ic_gauge_photo_placeholder)
                .into(gaugePhotoThumbnailImageView);
        rootView.requestLayout();
    }

}
