package de.geoapis.apps.mv.map;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.*;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.tundem.aboutlibraries.Libs;
import com.tundem.aboutlibraries.ui.LibsCompatActivity;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.Injector;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.WatersApp;
import de.geoapis.apps.mv.about.AboutActivity;
import de.geoapis.apps.mv.base.BaseActivity;
import de.geoapis.apps.mv.contract.BundleKeys;
import de.geoapis.apps.mv.customviews.InfoRow;
import de.geoapis.apps.mv.data.api.ApiModule;
import de.geoapis.apps.mv.data.api.FederalState;
import de.geoapis.apps.mv.data.api.GaugeFeaturesService;
import de.geoapis.apps.mv.data.model.gauges.*;
import de.geoapis.apps.mv.data.model.streams.ReadStreamFeaturesResponse;
import de.geoapis.apps.mv.devinfo.DeveloperInformationActivity;
import de.geoapis.apps.mv.map.model.GaugeItem;
import de.geoapis.apps.mv.prefs.PreferencesHelper;
import de.geoapis.apps.mv.reports.NewReportActivity;
import de.geoapis.apps.mv.utils.BoundingBox;
import de.geoapis.apps.mv.waterlevels.WaterLevelsActivity;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import org.parceler.Parcels;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MapsActivity extends BaseActivity
        implements GoogleMap.OnCameraChangeListener,
        GoogleMap.OnMapClickListener,
        ClusterManager.OnClusterItemClickListener<GaugeItem> {

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Inject
    @Named(ApiModule.GAUGE_FEATURES)
    GaugeFeaturesService mGaugeFeaturesService;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private View mMapView;
    private Subscription mGaugeFeaturesSubscription;
    private Subscription mStreamFeaturesSubscription;
    private ClusterManager<GaugeItem> mClusterManager;
    private SlidingUpPanelLayout mSlidingUpPanelLayout;
    private GaugeItem mSelectedGaugeItem;
    private de.geoapis.apps.mv.data.model.streams.Feature mSelectedStreamFeature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.inject(this);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        mSlidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        SimplePanelSlideListener mSimplePanelSlideListener = new SimplePanelSlideListener();
        mSlidingUpPanelLayout.setPanelSlideListener(mSimplePanelSlideListener);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onPause() {
        cancelSubscriptions(mGaugeFeaturesSubscription);
        cancelSubscriptions(mStreamFeaturesSubscription);
        if (mMap != null) {
            CameraPosition cameraPosition = mMap.getCameraPosition();
            if (cameraPosition != null) {
                mPreferencesHelper.storeCameraPosition(cameraPosition);
            }
        }
        super.onPause();
    }

    private void cancelSubscriptions(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onBackPressed() {
        if (mSlidingUpPanelLayout != null && (
                mSlidingUpPanelLayout.isPanelExpanded() ||
                        mSlidingUpPanelLayout.isPanelAnchored())) {
            mSlidingUpPanelLayout.collapsePanel();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        if (mSelectedGaugeItem != null) {
            outState.putParcelable(BundleKeys.SELECTED_MARKER, Parcels.wrap(mSelectedGaugeItem));
        }
        if (mSelectedStreamFeature != null) {
            outState.putParcelable(BundleKeys.SELECTED_STREAM, Parcels.wrap(mSelectedStreamFeature));
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(BundleKeys.SELECTED_MARKER)) {
            Parcelable parcelable = savedInstanceState.getParcelable(BundleKeys.SELECTED_MARKER);
            if (parcelable == null) {
                Log.e(getClass().getName(), "Selected marker cannot be restored.");
                return;
            }
            // Simulate marker selection for future onSaveInstanceState
            mSelectedGaugeItem = Parcels.unwrap(parcelable);
        }
        if (savedInstanceState.containsKey(BundleKeys.SELECTED_STREAM)) {
            Parcelable parcelable = savedInstanceState.getParcelable(BundleKeys.SELECTED_STREAM);
            if (parcelable == null) {
                Log.e(getClass().getName(), "Selected stream cannot be restored.");
                return;
            }
            // Simulate stream selection for future onSaveInstanceState
            mSelectedStreamFeature = Parcels.unwrap(parcelable);
        }
        setUpDragViewContent();
        updateGaugeInfo();
        updateStreamInfo();
        updateSubtitle();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment mapFragment =
                    (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mMap = mapFragment.getMap();
            mMapView = mapFragment.getView();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.setOnCameraChangeListener(this);
        mClusterManager = new ClusterManager<>(this, mMap);
        mClusterManager.setRenderer(new GaugeItemRenderer(this, mMap, mClusterManager));
        mMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterItemClickListener(this);
        mMap.setOnMapClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(
                MagunTileProviderFactory.getMagunTileProvider(
                        MagunTileProviderFactory.RESOLUTION.ROUGH)
        ));
        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(
                MagunTileProviderFactory.getMagunTileProvider(
                        MagunTileProviderFactory.RESOLUTION.MEDIUM)
        ));
        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(
                MagunTileProviderFactory.getMagunTileProvider(
                        MagunTileProviderFactory.RESOLUTION.FINE)
        ));
        restorePreviouslyChosenMapType();

        // Position map: Use last known or initial camera position
        CameraUpdate cameraUpdate;
        if (mPreferencesHelper.storesCameraPosition()) {
            CameraPosition cameraPosition = mPreferencesHelper.restoreCameraPosition();
            cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        } else {
            LatLngBounds initialBounds = getInitialLayoutBounds();
            cameraUpdate = CameraUpdateFactory.newLatLngBounds(initialBounds, 10);
        }
        animateCamera(cameraUpdate);
    }

    private void animateCamera(final CameraUpdate cameraUpdate) {
        if (mMapView.getViewTreeObserver().isAlive()) {
            mMapView.getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @SuppressWarnings("deprecation")
                        @Override
                        public void onGlobalLayout() {
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                mMapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            } else {
                                mMapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                            try {
                                mMap.animateCamera(cameraUpdate);
                            } catch (IllegalStateException e) {
                                Log.e(getClass().getName(), e.getLocalizedMessage());
                                mMap.setOnMapLoadedCallback(() -> mMap.animateCamera(cameraUpdate));
                            }
                        }
                    });
        }
    }

    private LatLngBounds getInitialLayoutBounds() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(WatersApp.BOUNDS_NORTH_WEST);
        builder.include(WatersApp.BOUNDS_SOUTH_EAST);
        return builder.build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.maps, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_report:
                openNewReport();
                return true;
            case R.id.action_show_water_levels_list:
                openWaterLevelsList();
                return true;
            case R.id.action_map_type:
                openMapTypeChooser();
                return true;
            case R.id.action_developer_info:
                openDeveloperInformation();
                return true;
            case R.id.action_libraries:
                openLibrariesInformation();
                return true;
            case R.id.action_about:
                openAbout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_new_report);
        toggleMarkerAssociatedMenuItem(item);
        item = menu.findItem(R.id.action_show_water_levels_list);
        toggleMarkerAssociatedMenuItem(item);
        return super.onPrepareOptionsMenu(menu);
    }

    private void toggleMarkerAssociatedMenuItem(MenuItem item) {
        if (mSelectedGaugeItem == null) {
            // No marker is selected
            item.setVisible(false);
        } else {
            // A marker item was selected
            item.setVisible(true);
        }
    }

    private void toggleMarkerAssociatedMenuItem() {
        // Triggers onPrepareOptionsMenu to be called
        supportInvalidateOptionsMenu();
    }

    private void openNewReport() {
        Intent intent = new Intent(this, NewReportActivity.class);
        intent.putExtra(BundleKeys.SELECTED_MARKER, Parcels.wrap(mSelectedGaugeItem));
        startActivity(intent);
    }

    private void openWaterLevelsList() {
        Intent intent = new Intent(this, WaterLevelsActivity.class);
        intent.putExtra(BundleKeys.SELECTED_MARKER, Parcels.wrap(mSelectedGaugeItem));
        startActivity(intent);
    }

    private void openLibrariesInformation() {
        Intent intent = new Intent(getApplicationContext(), LibsCompatActivity.class);
        intent.putExtra(Libs.BUNDLE_FIELDS, Libs.toStringArray(R.string.class.getFields()));
        intent.putExtra(Libs.BUNDLE_LIBS, new String[]{
                "butterknife",
                "crouton",
                "dagger",
                "okhttp",
                "picasso",
                "retrofit",
                "androidslidinguppanel"
        });

        intent.putExtra(Libs.BUNDLE_AUTODETECT, false);
        intent.putExtra(Libs.BUNDLE_VERSION, true);
        intent.putExtra(Libs.BUNDLE_LICENSE, true);
        intent.putExtra(Libs.BUNDLE_TITLE, getString(R.string.title_activity_libraries));
        intent.putExtra(Libs.BUNDLE_THEME, R.style.AppTheme);
        startActivity(intent);
    }

    private void loadGaugeFeatures() {
        if (!isOnline()) {
            Crouton.makeText(this, R.string.status_offline, Style.ALERT).show();
            return;
        }
        String featureTypes = BuildConfig.GAUGE_FEATURES_API_FEATURE_TYPE_GAUGE_MECKLENBURG_VORPOMMERN;
        LatLngBounds latLngBounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        String filter = getBoundingBoxFilter(new BoundingBox(latLngBounds), featureTypes);
        Observable<ReadGaugeFeaturesResponse> responseObservable = mGaugeFeaturesService
                .readGaugeFeatures(FederalState.MV, filter);
        mGaugeFeaturesSubscription = AppObservable.bindActivity(this, responseObservable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(ReadGaugeFeaturesResponse::getResult)
                .map(Result::getFeatures)
                .subscribe(new Subscriber<List<Feature>>() {
                    @Override
                    public void onCompleted() {
                        // Nothing to do here
                    }

                    @Override
                    public void onError(final Throwable e) {
                        Log.e(getClass().getName(), e.getMessage());
                    }

                    @Override
                    public void onNext(final List<Feature> features) {
                        replaceAllMarkers(features);
                    }
                });
    }

    private void loadStreamFeatures(BoundingBox boundingBox) {
        if (!isOnline()) {
            Crouton.makeText(this, R.string.status_offline, Style.ALERT).show();
            return;
        }
        String featureTypes = BuildConfig.GAUGE_FEATURES_API_FEATURE_TYPE_STREAMS;
        String filter = getBoundingBoxFilter(boundingBox, featureTypes);
        Observable<ReadStreamFeaturesResponse> responseObservable = mGaugeFeaturesService
                .readStreamFeatures(FederalState.MV, filter);
        mStreamFeaturesSubscription = AppObservable.bindActivity(this, responseObservable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(ReadStreamFeaturesResponse::getResult)
                .map(de.geoapis.apps.mv.data.model.streams.Result::getFeatures)
                .subscribe(new Subscriber<List<de.geoapis.apps.mv.data.model.streams.Feature>>() {
                    @Override
                    public void onCompleted() {
                        updateStreamInfo();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(getClass().getName(), e.getMessage());
                    }

                    @Override
                    public void onNext(final List<de.geoapis.apps.mv.data.model.streams.Feature> features) {
                        mSelectedStreamFeature = features.get(0);
                    }
                });
    }

    private String getBoundingBoxFilter(final BoundingBox boundingBox, final String featureTypes) {
        double southWestLatitude = boundingBox.getSouthWest().latitude;
        double southWestLongitude = boundingBox.getSouthWest().longitude;
        double northEastLatitude = boundingBox.getNorthEast().latitude;
        double northEastLongitude = boundingBox.getNorthEast().longitude;
        return getString(R.string.bounding_box_filter,
                southWestLongitude,
                southWestLatitude,
                northEastLongitude,
                northEastLatitude,
                featureTypes);
    }

    private void replaceAllMarkers(List<Feature> features) {
        mClusterManager.clearItems();
        List<GaugeItem> gaugeItems = new ArrayList<>(features.size());
        for (Feature feature : features) {
            Point point = feature.getGeo().getPoint();
            LatLng position = new LatLng(point.getLat(), point.getLon());
            GaugeItem gaugeItem = getGaugeItem(feature.getId(), position, feature.getProperties());
            gaugeItems.add(gaugeItem);
        }
        mClusterManager.addItems(gaugeItems);
        mClusterManager.cluster();
    }

    private GaugeItem getGaugeItem(int featureId, LatLng position, Properties properties) {
        return new GaugeItem(
                featureId,
                position,
                "" + featureId, // properties.getGaugeCode(),
                properties.getGaugeName(),
                properties.getGaugeNumberWsa(),
                properties.getAreaKeyLawa(),
                properties.getWaterKeyLawa(),
                properties.getWaterNameLawaRoute(),
                properties.getGaugeDatum(),
                properties.getGaugeDatumHeightSystem(),
                properties.getLakeName(),
                properties.getWaterTypeGauge(this),
                properties.getEasting(),
                properties.getNorthing(),
                properties.getTimeSeriesStart(),
                properties.getTimeSeriesRange(),
                properties.getPrincipalValueNw(),
                properties.getPrincipalValueMnw(),
                properties.getPrincipalValueMw(),
                properties.getPrincipalValueMhw(),
                properties.getPrincipalValueHw(),
                properties.getPrincipalValueNnw(),
                properties.getPrincipalValueHhw(),
                properties.getPrincipalValueNq(),
                properties.getPrincipalValueMnq(),
                properties.getPrincipalValueMq(),
                properties.getPrincipalValueMhq(),
                properties.getPrincipalValueHq(),
                properties.getPrincipalValueNnq(),
                properties.getPrincipalValueHhq(),
                properties.getDrainageArea(),
                properties.getDirectDrainageArea(),
                properties.getDirectDrainageAreaPercentual()
        );
    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    private void openDeveloperInformation() {
        startActivity(new Intent(this, DeveloperInformationActivity.class));
    }

    private void openAbout() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    private void openMapTypeChooser() {
        // Normal is default
        int selectedMapTypeIndex = getMapTypeIndex(getString(R.string.map_type_normal));
        // Override default if set before
        if (mPreferencesHelper.storesMapTypeIndex()) {
            selectedMapTypeIndex = mPreferencesHelper.restoreMapTypeIndex();
        }
        final AtomicInteger tempSelectedItemIndex = new AtomicInteger(selectedMapTypeIndex);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.pick_map_type)
                .setSingleChoiceItems(R.array.map_type_names, selectedMapTypeIndex,
                        (dialog, itemIndex) -> tempSelectedItemIndex.set(itemIndex))
                .setPositiveButton(R.string.ok,
                        (dialog, whichButton) -> {
                            switchMapType(tempSelectedItemIndex.get());
                            storeMapTypeSelection(tempSelectedItemIndex.get());
                        })
                .setNegativeButton(R.string.cancel,
                        (dialog, whichButton) -> {
                            // Nothing to do here
                        });
        builder.show();
    }

    private void storeMapTypeSelection(int selectedMapTypeIndex) {
        mPreferencesHelper.storeMapTypeIndex(selectedMapTypeIndex);
    }

    private void restorePreviouslyChosenMapType() {
        if (mPreferencesHelper.storesMapTypeIndex()) {
            int selectedMapTypeIndex = mPreferencesHelper.restoreMapTypeIndex();
            switchMapType(selectedMapTypeIndex);
        }
    }

    private void switchMapType(int selectedMapTypeIndex) {
        if (mMap == null) {
            return;
        }
        String[] mapTypeNames = getResources().getStringArray(R.array.map_type_names);
        String mapTypeName = mapTypeNames[selectedMapTypeIndex];
        if (mapTypeName.equals(getString(R.string.map_type_normal))) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
        if (mapTypeName.equals((getString(R.string.map_type_satellite)))) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }
    }

    private int getMapTypeIndex(final String mapTypeName) {
        String[] mapTypeNames = getResources().getStringArray(R.array.map_type_names);
        for (int i = 0; i < mapTypeNames.length; ++i) {
            String currentMapTypeName = mapTypeNames[i];
            if (currentMapTypeName.equals(mapTypeName)) {
                return i;
            }
        }
        throw new IllegalStateException("Unknown map type name: " + mapTypeName);
    }

    @Override
    public void onMapClick(final LatLng latLng) {
        // Reset selection when map is clicked
        mSelectedGaugeItem = null;
        if (mSlidingUpPanelLayout != null) {
            mSlidingUpPanelLayout.hidePanel();
        }
        toggleMarkerAssociatedMenuItem();
        setUpDragViewContent();
        updateSubtitle();
        loadStreamFeatures(BoundingBox.createBoundingBox(latLng));
    }

    @Override
    public void onCameraChange(final CameraPosition cameraPosition) {
        mClusterManager.onCameraChange(cameraPosition);
        loadGaugeFeatures();
    }

    @Override
    public boolean onClusterItemClick(final GaugeItem gaugeItem) {
        // Store selection for future onSaveInstanceState
        mSelectedGaugeItem = gaugeItem;
        mSelectedStreamFeature = null;
        toggleMarkerAssociatedMenuItem();
        setUpDragViewContent();
        updateGaugeInfo();
        updateSubtitle();
        return false;
    }

    private void setUpDragViewContent() {
        if (mSelectedGaugeItem != null) {
            ViewGroup dragView = (ViewGroup) findViewById(R.id.drag_view);
            dragView.removeAllViews();
            LayoutInflater.from(this).inflate(R.layout.gauge_info, dragView);
        } else {
            ViewGroup dragView = (ViewGroup) findViewById(R.id.drag_view);
            dragView.removeAllViews();
            LayoutInflater.from(this).inflate(R.layout.stream_info, dragView);
        }
    }

    private void updateGaugeInfo() {
        if (mSelectedGaugeItem == null) {
            return;
        }

        // Gauge position
        InfoRow view = (InfoRow) findViewById(R.id.gauge_info_gauge_position);
        String text = getString(R.string.label_gauge_position);
        view.setLabelText(text);
        text = getString(R.string.value_gauge_position,
                mSelectedGaugeItem.getPosition().latitude, mSelectedGaugeItem.getPosition().longitude);
        view.setValueText(text);

        InfoRow.setUpInfoRow(this, R.id.gauge_info_gauge_name,
                R.string.label_gauge_name,
                mSelectedGaugeItem.getGaugeName());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_gauge_code,
                R.string.label_gauge_code,
                mSelectedGaugeItem.getGaugeCode());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_gauge_number_wsa,
                R.string.label_gauge_number_wsa,
                mSelectedGaugeItem.getGaugeNumberWsa());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_area_key_lawa,
                R.string.label_area_key_lawa,
                mSelectedGaugeItem.getAreaKeyLawa());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_water_key_lawa,
                R.string.label_water_key_lawa,
                mSelectedGaugeItem.getWaterKeyLawa());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_water_name_lawa_route,
                R.string.label_water_name_lawa_route,
                mSelectedGaugeItem.getWaterNameLavaRoute());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_gauge_datum,
                R.string.label_gauge_datum,
                mSelectedGaugeItem.getGaugeDatum());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_gauge_datum_height_system,
                R.string.label_gauge_datum_height_system,
                mSelectedGaugeItem.getGaugeDatumHeightSystem());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_lake_name,
                R.string.label_lake_name,
                mSelectedGaugeItem.getLakeName());

        text = mSelectedGaugeItem.getWaterTypeGauge();
        InfoRow.setUpInfoRow(this, R.id.gauge_info_water_type_gauge,
                R.string.label_water_type_gauge,
                text);

        InfoRow.setUpInfoRow(this, R.id.gauge_info_easting,
                R.string.label_easting,
                mSelectedGaugeItem.getEasting());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_northing,
                R.string.label_northing,
                mSelectedGaugeItem.getNorthing());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_time_series_start,
                R.string.label_time_series_start,
                mSelectedGaugeItem.getTimeSeriesStart());

        InfoRow.setUpInfoRow(this, R.id.gauge_info_time_series_range,
                R.string.label_time_series_range,
                mSelectedGaugeItem.getTimeSeriesRange());

        // Pegelhauptwerte

        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_nw,
                R.string.label_principal_value_nw,
                mSelectedGaugeItem.getPrincipalValueNw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_mnw,
                R.string.label_principal_value_mnw,
                mSelectedGaugeItem.getPrincipalValueMnw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_mw,
                R.string.label_principal_value_mw,
                mSelectedGaugeItem.getPrincipalValueMw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_mhw,
                R.string.label_principal_value_mhw,
                mSelectedGaugeItem.getPrincipalValueMhw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_hw,
                R.string.label_principal_value_hw,
                mSelectedGaugeItem.getPrincipalValueHw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_nnw,
                R.string.label_principal_value_nnw,
                mSelectedGaugeItem.getPrincipalValueNnw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_hhw,
                R.string.label_principal_value_hhw,
                mSelectedGaugeItem.getPrincipalValueHhw(),
                R.string.value_principal_value_water_level);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_nq,
                R.string.label_principal_value_nq,
                mSelectedGaugeItem.getPrincipalValueNq(),
                R.string.value_principal_value_flow);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_mnq,
                R.string.label_principal_value_mnq,
                mSelectedGaugeItem.getPrincipalValueMnq(),
                R.string.value_principal_value_flow);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_mq,
                R.string.label_principal_value_mq,
                mSelectedGaugeItem.getPrincipalValueMq(),
                R.string.value_principal_value_flow);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_mhq,
                R.string.label_principal_value_mhq,
                mSelectedGaugeItem.getPrincipalValueMhq(),
                R.string.value_principal_value_flow);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_hq,
                R.string.label_principal_value_hq,
                mSelectedGaugeItem.getPrincipalValueHq(),
                R.string.value_principal_value_flow);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_nnq,
                R.string.label_principal_value_nnq,
                mSelectedGaugeItem.getPrincipalValueNnq(),
                R.string.value_principal_value_flow);
        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_principal_value_hhq,
                R.string.label_principal_value_hhq,
                mSelectedGaugeItem.getPrincipalValueHhq(),
                R.string.value_principal_value_flow);

        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_drainage_area,
                R.string.label_drainage_area,
                mSelectedGaugeItem.getDrainageArea(),
                R.string.value_drainage_area);

        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_direct_drainage_area,
                R.string.label_direct_drainage_area,
                mSelectedGaugeItem.getDirectDrainageArea(),
                R.string.value_direct_drainage_area);

        InfoRow.setUpInfoRowWithUnit(this, R.id.gauge_info_direct_drainage_area_percentual,
                R.string.label_direct_drainage_area_percentual,
                mSelectedGaugeItem.getDirectDrainageAreaPercentual(),
                R.string.value_direct_drainage_area_percentual);

        if (mSlidingUpPanelLayout != null) {
            mSlidingUpPanelLayout.showPanel();
        }
    }

    private void updateStreamInfo() {
        if (mSelectedStreamFeature == null) {
            return;
        }

        de.geoapis.apps.mv.data.model.streams.Properties properties =
                mSelectedStreamFeature.getProperties();

        InfoRow.setUpInfoRow(this, R.id.stream_info_area_key_lawa,
                R.string.label_area_key_lawa,
                properties.getAreaKeyLawa());
        InfoRow.setUpInfoRow(this, R.id.stream_info_water_key_lawa,
                R.string.label_water_key_lawa,
                properties.getWaterKeyLawa());
        InfoRow.setUpInfoRow(this, R.id.stream_info_topographic_stream_name_1,
                R.string.label_topographic_stream_name_1,
                properties.getTopographicStreamName1());
        InfoRow.setUpInfoRow(this, R.id.stream_info_topographic_stream_name_2,
                R.string.label_topographic_stream_name_2,
                properties.getTopographicStreamName2());
        InfoRow.setUpInfoRow(this, R.id.stream_info_waterway_name,
                R.string.label_waterway_name,
                properties.getWaterwayName());
        String text = getString(R.string.value_segment_length,
                properties.getSegmentLength());
        InfoRow.setUpInfoRow(this, R.id.stream_info_segment_length,
                R.string.label_segment_length,
                text);
        InfoRow.setUpInfoRow(this, R.id.stream_info_country,
                R.string.label_country,
                properties.getCountry());
        InfoRow.setUpInfoRow(this, R.id.stream_info_object_type,
                R.string.label_object_type,
                properties.getObjectType(this));
        InfoRow.setUpInfoRow(this, R.id.stream_info_surface_location,
                R.string.label_surface_location,
                properties.getSurfaceLocation(this));
        InfoRow.setUpInfoRow(this, R.id.stream_info_wbv_name,
                R.string.label_wbv_name,
                properties.getWbvName());
        InfoRow.setUpInfoRow(this, R.id.stream_info_water_category,
                R.string.label_water_category,
                properties.getWaterCategory(this));
        InfoRow.setUpInfoRow(this, R.id.stream_info_wrrl_reportable_stream,
                R.string.label_wrrl_reportable_stream,
                properties.getWrrlReportableStream(this));

        if (mSlidingUpPanelLayout != null) {
            mSlidingUpPanelLayout.showPanel();
        }
    }

    private void updateSubtitle() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(
                    mSelectedGaugeItem == null ? null : mSelectedGaugeItem.getGaugeName());
        }
    }

    private class SimplePanelSlideListener implements SlidingUpPanelLayout.PanelSlideListener {

        @Override
        public void onPanelSlide(View view, float v) {
            // Nothing to do here, dude!
        }

        @Override
        public void onPanelCollapsed(View view) {
            toggleMarkerAssociatedMenuItem();
        }

        @Override
        public void onPanelExpanded(View view) {
            toggleMarkerAssociatedMenuItem();
        }

        @Override
        public void onPanelAnchored(View view) {
            toggleMarkerAssociatedMenuItem();
        }

        @Override
        public void onPanelHidden(View view) {
            toggleMarkerAssociatedMenuItem();
        }
    }

}
