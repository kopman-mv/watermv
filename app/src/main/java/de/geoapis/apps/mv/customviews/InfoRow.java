package de.geoapis.apps.mv.customviews;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import de.geoapis.apps.mv.R;

public class InfoRow extends RelativeLayout {

    protected TextView mLabelTextView;
    protected TextView mValueTextView;

    public InfoRow(final Context context) {
        super(context);
        init(context, null, 0);
    }

    public InfoRow(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public InfoRow(final Context context, final AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    protected void init(final Context context, final AttributeSet attrs, int defStyle) {
        TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.InfoRow, defStyle, 0);

        // Get default layout weights
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.info_row_default_label_layout_weight, outValue, true);
        float defaultLabelLayoutWeight = outValue.getFloat();
        getResources().getValue(R.dimen.info_row_default_value_layout_weight, outValue, true);
        float defaultValueLayoutWeight = outValue.getFloat();

        // Get specific layout weights
        float labelLayoutWeight;
        float valueLayoutWeight;
        try {
            labelLayoutWeight = styledAttributes.getFloat(
                    R.styleable.InfoRow_label_layout_weight, defaultLabelLayoutWeight);
            valueLayoutWeight = styledAttributes.getFloat(
                    R.styleable.InfoRow_value_layout_weight, defaultValueLayoutWeight);
        } finally {
            styledAttributes.recycle();
        }

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.info_row, this, true);

        // Apply custom layout weight for label
        mLabelTextView = (TextView) layout.findViewById(R.id.info_row_label);
        ViewGroup.LayoutParams xmlDefinedLayoutParams = mLabelTextView.getLayoutParams();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                xmlDefinedLayoutParams.width, xmlDefinedLayoutParams.height, labelLayoutWeight);
        mLabelTextView.setLayoutParams(layoutParams);

        // Apply custom layout weight for value
        mValueTextView = (TextView) layout.findViewById(R.id.info_row_value);
        xmlDefinedLayoutParams = mValueTextView.getLayoutParams();
        layoutParams = new LinearLayout.LayoutParams(
                xmlDefinedLayoutParams.width, xmlDefinedLayoutParams.height, valueLayoutWeight);
        mValueTextView.setLayoutParams(layoutParams);
    }

    public void setLabelText(final String text) {
        mLabelTextView.setText(text);
    }

    public void setValueText(final String text) {
        mValueTextView.setText(text);
    }

    public CharSequence getLabelText() {
        return mLabelTextView.getText();
    }

    public CharSequence getValueText() {
        return mValueTextView.getText();
    }

    public static void setUpInfoRow(Activity activity,
                                    int viewResId,
                                    int labelTextResId,
                                    String valueText) {
        if (valueText == null || TextUtils.isEmpty(valueText)) {
            valueText = activity.getString(R.string.value_no_value_placeholder);
        }
        InfoRow view = (InfoRow) activity.findViewById(viewResId);
        String text = activity.getString(labelTextResId);
        view.setLabelText(text);
        view.setValueText(valueText);
    }

    public static void setUpInfoRowWithUnit(Activity activity,
                                            int viewResId,
                                            int labelTextResId,
                                            String valueText,
                                            int valueWithUnitResId) {
        InfoRow view = (InfoRow) activity.findViewById(viewResId);
        String text = activity.getString(labelTextResId);
        if (valueText == null || TextUtils.isEmpty(valueText)) {
            valueText = activity.getString(R.string.value_no_value_placeholder);
        }
        else {
            valueText = activity.getString(valueWithUnitResId, valueText);
        }
        view.setLabelText(text);
        view.setValueText(valueText);
    }

}
