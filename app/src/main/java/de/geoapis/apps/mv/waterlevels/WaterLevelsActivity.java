package de.geoapis.apps.mv.waterlevels;

import android.os.Bundle;
import android.view.MenuItem;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.base.BaseActivity;

public class WaterLevelsActivity extends BaseActivity {

    public static final String BACK_STACK_STATE_WATER_LEVELS_LIST_IS_TOP =
            BuildConfig.APPLICATION_ID + "BACK_STACK_STATE_WATER_LEVELS_LIST_IS_TOP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_levels_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            addFragment(R.id.water_levels_list_container,
                    new WaterLevelsListFragment(),
                    WaterLevelsListFragment.FRAGMENT_TAG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_show_water_levels_list:
                showWaterLevelsList();
                return true;
            case R.id.action_show_water_levels_chart:
                showWaterLevelsChart();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showWaterLevelsList() {
        restoreFragment(BACK_STACK_STATE_WATER_LEVELS_LIST_IS_TOP);
    }

    private void showWaterLevelsChart() {
        replaceFragment(R.id.water_levels_list_container,
                new WaterLevelsChartFragment(),
                WaterLevelsChartFragment.FRAGMENT_TAG,
                BACK_STACK_STATE_WATER_LEVELS_LIST_IS_TOP);
    }

}
