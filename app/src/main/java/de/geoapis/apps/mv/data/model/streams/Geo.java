package de.geoapis.apps.mv.data.model.streams;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.parceler.Parcel;
import org.parceler.Transient;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

@Parcel
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "area"
})
public class Geo {

    @JsonProperty("area")
    public Area area;

    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Geo() {
        // Required by Parceler
    }

    /**
     * @return The area
     */
    @JsonProperty("area")
    public Area getArea() {
        return area;
    }

    /**
     * @param area The area
     */
    @JsonProperty("area")
    public void setArea(Area area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
