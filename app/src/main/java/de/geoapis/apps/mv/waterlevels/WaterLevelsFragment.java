package de.geoapis.apps.mv.waterlevels;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import de.geoapis.apps.mv.Injector;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.base.BaseFragment;
import de.geoapis.apps.mv.contract.BundleKeys;
import de.geoapis.apps.mv.data.api.ApiModule;
import de.geoapis.apps.mv.data.api.GaugeReportsService;
import de.geoapis.apps.mv.data.model.HistoricGaugeReport;
import de.geoapis.apps.mv.data.model.ReadCrowdSourcingGaugeReportsResponse;
import de.geoapis.apps.mv.data.model.RealTimeGaugeReport;
import de.geoapis.apps.mv.map.model.GaugeItem;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import org.parceler.Parcels;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

public abstract class WaterLevelsFragment extends BaseFragment {

    @Inject
    @Named(ApiModule.GAUGE_REPORTS)
    GaugeReportsService mGaugeReportsService;

    protected Subscription mGaugeReportsSubscription;

    private Subscription mRealTimeGaugeReportsSubscription;

    private Subscription mHistoricGaugeReportsSubscription;

    protected GaugeItem mSelectedGaugeItem;

    protected List<ReadCrowdSourcingGaugeReportsResponse> mCrowdSourcingGaugeReports;

    protected List<RealTimeGaugeReport> mRealTimeGaugeReports;

    protected List<HistoricGaugeReport> mHistoricGaugeReports;

    @Override
    public void onPause() {
        cancelSubscriptions(mGaugeReportsSubscription);
        cancelSubscriptions(mRealTimeGaugeReportsSubscription);
        cancelSubscriptions(mHistoricGaugeReportsSubscription);
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Injector.inject(this);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey(BundleKeys.SELECTED_MARKER)) {
                Parcelable parcelable = extras.getParcelable(BundleKeys.SELECTED_MARKER);
                mSelectedGaugeItem = Parcels.unwrap(parcelable);
                if (mSelectedGaugeItem == null) {
                    throw new NullPointerException("Gauge item is null.");
                }
                updateSubtitle();
                loadCrowdSourcingReports();
                loadRealTimeGaugeReports();
                loadHistoricGaugeReports();
            }
        }
    }

    protected void updateSubtitle() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(
                    mSelectedGaugeItem == null ? null : mSelectedGaugeItem.getGaugeName());
        }
    }

    protected void loadCrowdSourcingReports() {
        Observable<List<ReadCrowdSourcingGaugeReportsResponse>> responseObservable =
                mGaugeReportsService.readCrowdSourcingGaugeReports(mSelectedGaugeItem.getFeatureId());
        mGaugeReportsSubscription = AppObservable.bindFragment(this, responseObservable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ReadCrowdSourcingGaugeReportsResponse>>() {
                    @Override
                    public void onCompleted() {
                        updateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(getClass().getName(), e.getMessage());
                        showCrowdSourcingReportsNotAvailableMessage();
                        updateView();
                    }

                    @Override
                    public void onNext(List<ReadCrowdSourcingGaugeReportsResponse> readCrowdSourcingGaugeReportsResponse) {
                        mCrowdSourcingGaugeReports = readCrowdSourcingGaugeReportsResponse;
                        if (mCrowdSourcingGaugeReports == null || mCrowdSourcingGaugeReports.isEmpty()) {
                            showCrowdSourcingReportsNotAvailableMessage();
                        }
                    }
                });
    }

    protected void loadRealTimeGaugeReports() {
        Observable<List<List<Long>>> responseObservable =
                mGaugeReportsService.readRealTimeGaugeReports(mSelectedGaugeItem.getFeatureId());
        mRealTimeGaugeReportsSubscription = AppObservable.bindFragment(this, responseObservable)
                .subscribeOn(Schedulers.io())
                .flatMap(Observable::from)
                .map(innerList -> new RealTimeGaugeReport(innerList.get(0), innerList.get(1)))
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<RealTimeGaugeReport>>() {
                    @Override
                    public void onCompleted() {
                        updateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(getClass().getName(), e.getMessage());
                        showRealTimeReportsNotAvailableMessage();
                        updateView();
                    }

                    @Override
                    public void onNext(List<RealTimeGaugeReport> realTimeGaugeReports) {
                        mRealTimeGaugeReports = realTimeGaugeReports;
                        if (mRealTimeGaugeReports == null || mRealTimeGaugeReports.isEmpty()) {
                            showRealTimeReportsNotAvailableMessage();
                        }
                    }
                });
    }

    protected void loadHistoricGaugeReports() {
        Observable<List<List<Long>>> responseObservable =
                mGaugeReportsService.readHistoricGaugeReports(mSelectedGaugeItem.getFeatureId());
        mHistoricGaugeReportsSubscription = AppObservable.bindFragment(this, responseObservable)
                .subscribeOn(Schedulers.io())
                .flatMap(Observable::from)
                .map(innerList -> new HistoricGaugeReport(innerList.get(0), innerList.get(1)))
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<HistoricGaugeReport>>() {
                    @Override
                    public void onCompleted() {
                        updateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(getClass().getName(), e.getMessage());
                        showHistoricReportsNotAvailableMessage();
                        updateView();
                    }

                    @Override
                    public void onNext(List<HistoricGaugeReport> historicGaugeReports) {
                        mHistoricGaugeReports = historicGaugeReports;
                        if (mHistoricGaugeReports == null || mHistoricGaugeReports.isEmpty()) {
                            showHistoricReportsNotAvailableMessage();
                        }
                    }
                });
    }

    protected abstract void updateView();

    private void showCrowdSourcingReportsNotAvailableMessage() {
        String message = getString(R.string.water_levels_error_loading_crowd_sourcing_reports);
        Crouton.makeText(getActivity(), message, Style.INFO).show();
    }

    private void showRealTimeReportsNotAvailableMessage() {
        String message = getString(R.string.water_levels_error_loading_real_time_reports);
        Crouton.makeText(getActivity(), message, Style.INFO).show();
    }

    private void showHistoricReportsNotAvailableMessage() {
        String message = getString(R.string.water_levels_error_loading_historic_reports);
        Crouton.makeText(getActivity(), message, Style.INFO).show();
    }

    private void cancelSubscriptions(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

}
