package de.geoapis.apps.mv.base;

import android.support.v4.app.Fragment;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    // Call from within onCreateView()
    public void inject(View rootView) {
        ButterKnife.inject(this, rootView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

}
