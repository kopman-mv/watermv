package de.geoapis.apps.mv.data.model;


import de.geoapis.apps.mv.BuildConfig;

public class GaugeReport {

    public String gauge;
    public String waterLevel;
    public String userName;
    public String emailAddress;
    public String recordedAt;

    public GaugeReport(int gauge,
                       String waterLevel,
                       String userName,
                       String emailAddress,
                       String recordedAt) {
        this.gauge = BuildConfig.GAUGES_API_END_POINT + BuildConfig.GAUGES_API_READ_PATH + gauge + "/";
        this.waterLevel = waterLevel;
        this.userName = userName;
        this.emailAddress = emailAddress;
        this.recordedAt = recordedAt;
    }

    @Override
    public String toString() {
        return "gauge = " + gauge +
                ", waterLevel = " + waterLevel +
                ", userName = " + userName +
                ", emailAddress = " + emailAddress +
                ", recordedAt = " + recordedAt;
    }

}
