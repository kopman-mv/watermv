package de.geoapis.apps.mv.data;

import dagger.Module;
import de.geoapis.apps.mv.data.api.ApiModule;

@Module(
        includes = ApiModule.class,
        complete = false,
        library = true
)
public class DataModule {
}
