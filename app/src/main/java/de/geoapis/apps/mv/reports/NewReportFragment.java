package de.geoapis.apps.mv.reports;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.ImageView;
import butterknife.InjectView;
import butterknife.OnClick;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NumberRule;
import com.mobsandgeeks.saripaar.annotation.TextRule;
import de.geoapis.apps.mv.BuildConfig;
import de.geoapis.apps.mv.Injector;
import de.geoapis.apps.mv.R;
import de.geoapis.apps.mv.base.BaseFragment;
import de.geoapis.apps.mv.contract.BundleKeys;
import de.geoapis.apps.mv.customviews.InfoRow;
import de.geoapis.apps.mv.data.api.ApiModule;
import de.geoapis.apps.mv.data.api.GaugeReportsService;
import de.geoapis.apps.mv.data.model.CreateCrowdSourcingGaugeReportResponse;
import de.geoapis.apps.mv.data.model.GaugeReport;
import de.geoapis.apps.mv.map.model.GaugeItem;
import de.geoapis.apps.mv.prefs.PreferencesHelper;
import de.geoapis.apps.mv.utils.DateTimeFormatter;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import org.joda.time.DateTime;
import org.parceler.Parcels;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;

public class NewReportFragment extends BaseFragment implements
        Validator.ValidationListener {

    public static final String FRAGMENT_TAG =
            BuildConfig.APPLICATION_ID + ".NEW_REPORT_FRAGMENT_FRAGMENT_TAG";

    protected static final String BUNDLE_KEY_GAUGE_PHOTO_PATH =
            BuildConfig.APPLICATION_ID + ".BUNDLE_KEY_GAUGE_PHOTO_PATH";

    protected static final String BUNDLE_KEY_GAUGE_PHOTO_BITMAP =
            BuildConfig.APPLICATION_ID + ".BUNDLE_KEY_GAUGE_PHOTO_BITMAP";

    @NumberRule(
            gt = 1,
            lt = 1000,
            order = 1,
            type = NumberRule.NumberType.INTEGER,
            messageResId = R.string.new_report_validation_water_level
    )
    @InjectView(R.id.new_report_water_level)
    EditText waterLevelEditText;

    @TextRule(
            order = 2,
            minLength = 2,
            maxLength = 200,
            messageResId = R.string.new_report_validation_user_name
    )
    @InjectView(R.id.new_report_user_name)
    EditText userNameEditText;

    @TextRule(
            order = 3,
            minLength = 5,
            maxLength = 200,
            messageResId = R.string.new_report_validation_email_address
    )
    @Email(
            order = 4,
            messageResId = R.string.new_report_validation_email_address
    )
    @InjectView(R.id.new_report_email_address)
    EditText emailAddressEditText;

    @InjectView(R.id.gauge_photo)
    ImageView gaugePhotoImageView;

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Inject
    @Named(ApiModule.GAUGE_REPORTS)
    GaugeReportsService mGaugeReportsService;

    private DateTime mRecordedAt;

    private Subscription mGaugeReportsSubscription;

    public static final int NEW_REPORT_FRAGMENT_REQUEST_CODE = 456;

    public static final int REQUEST_IMAGE_CAPTURE_RESULT_CODE = 1;

    private static final String IMAGE_FILE_PREFIX = "watersmv_";

    private static final String IMAGE_FILE_SUFFIX = ".jpg";

    private GaugeItem mSelectedGaugeItem;

    private String mGaugePhotoPath;

    private Bitmap mGaugePhotoBitmap;

    private Validator mValidator;

    public NewReportFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_report, container, false);
        setHasOptionsMenu(true);
        inject(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Injector.inject(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey(BundleKeys.SELECTED_MARKER)) {
                Parcelable parcelable = extras.getParcelable(BundleKeys.SELECTED_MARKER);
                mSelectedGaugeItem = Parcels.unwrap(parcelable);
                if (mSelectedGaugeItem == null) {
                    throw new NullPointerException("Gauge item is null.");
                }
                updateGaugeInformation();
            }
        }
        if (mPreferencesHelper.storesUserName()) {
            userNameEditText.setText(mPreferencesHelper.restoreUserName());
        }
        if (mPreferencesHelper.storesEmailAddress()) {
            emailAddressEditText.setText(mPreferencesHelper.restoreEmailAddress());
        }
    }

    @Override
    public void onPause() {
        if (mGaugeReportsSubscription != null && !mGaugeReportsSubscription.isUnsubscribed()) {
            mGaugeReportsSubscription.unsubscribe();
        }
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.new_report, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_submit_report) {
            mValidator.validate();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_REPORT_FRAGMENT_REQUEST_CODE &&
                resultCode == DateTimePickerFragment.DATE_TIME_PICKED_RESULT_CODE) {
            long recordedAtInMilliseconds = data.getLongExtra(
                    DateTimePickerFragment.DATE_TIME_PICKED_INTENT_KEY, 0);
            mRecordedAt = new DateTime(recordedAtInMilliseconds);
            updateRecordedAtTextView();
        } else if (requestCode == REQUEST_IMAGE_CAPTURE_RESULT_CODE &&
                resultCode == FragmentActivity.RESULT_OK) {
            generateGaugePhotoThumbnail();
            updateGaugePhotoImageView();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mGaugePhotoPath != null) {
            outState.putString(BUNDLE_KEY_GAUGE_PHOTO_PATH, mGaugePhotoPath);
            outState.putParcelable(BUNDLE_KEY_GAUGE_PHOTO_BITMAP, mGaugePhotoBitmap);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mGaugePhotoPath = savedInstanceState.getString(BUNDLE_KEY_GAUGE_PHOTO_PATH);
            mGaugePhotoBitmap = savedInstanceState.getParcelable(BUNDLE_KEY_GAUGE_PHOTO_BITMAP);
            updateGaugePhotoImageView();
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (mGaugePhotoPath == null || mGaugePhotoBitmap == null) {
            String message = getString(R.string.new_report_validation_gauge_photo);
            Crouton.makeText(getActivity(), message, Style.ALERT).show();
        } else {
            onSubmitReport();
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            ((EditText) failedView).setError(message);
        } else {
            Crouton.makeText(getActivity(), message, Style.ALERT).show();
        }
    }

    public void updateGaugeInformation() {
        FragmentActivity activity = getActivity();

        InfoRow.setUpInfoRow(activity, R.id.new_report_gauge_name,
                R.string.label_gauge_name, mSelectedGaugeItem.getGaugeName());

        InfoRow.setUpInfoRow(activity, R.id.new_report_lake_name,
                R.string.label_lake_name, mSelectedGaugeItem.getLakeName());

        InfoRow.setUpInfoRow(activity, R.id.new_report_gauge_code,
                R.string.label_gauge_code, mSelectedGaugeItem.getGaugeCode());

        // Gauge position
        InfoRow view = (InfoRow) activity.findViewById(R.id.new_report_gauge_position);
        String text = getString(R.string.label_gauge_position);
        view.setLabelText(text);
        text = getString(R.string.value_gauge_position,
                mSelectedGaugeItem.getPosition().latitude,
                mSelectedGaugeItem.getPosition().longitude);
        view.setValueText(text);

        // Recorded at
        mRecordedAt = DateTime.now();
        updateRecordedAtTextView();
    }

    public void updateRecordedAtTextView() {
        FragmentActivity activity = getActivity();
        InfoRow.setUpInfoRow(activity, R.id.new_report_recorded_at,
                R.string.label_recorded_at, DateTimeFormatter.getFormattedDateTime(
                        mRecordedAt, getString(R.string.date_time_display_pattern)));
    }

    @OnClick(R.id.new_report_recorded_at)
    public void onRecordedAtClick() {
        showDateTimePicker();
    }

    @OnClick(R.id.take_gauge_photo)
    public void onTakeGaugePhotoClick() {
        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        FragmentActivity activity = getActivity();
        PackageManager packageManager = activity.getPackageManager();
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                String message = getString(R.string.error_creating_temporary_image_file);
                Crouton.makeText(activity, message, Style.ALERT).show();
                Log.e(getClass().getName(),
                        "Error creating temporary image file: " + e.getMessage());
            }
            if (photoFile != null) {
                Uri photoFileUri = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_RESULT_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = DateTimeFormatter.getFormattedDateTime(DateTime.now(), "yyyyMMdd_HHmmss");
        String imageFileName = IMAGE_FILE_PREFIX + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(imageFileName, IMAGE_FILE_SUFFIX, storageDir);
        mGaugePhotoPath = imageFile.getAbsolutePath();
        return imageFile;
    }

    private void generateGaugePhotoThumbnail() {
        gaugePhotoImageView.setVisibility(View.VISIBLE);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 6;
        mGaugePhotoBitmap = BitmapFactory.decodeFile(mGaugePhotoPath, options);
    }

    private void updateGaugePhotoImageView() {
        if (mGaugePhotoBitmap != null) {
            gaugePhotoImageView.setImageBitmap(mGaugePhotoBitmap);
        }
    }

    private void showDateTimePicker() {
        DialogFragment dialogFragment = new DateTimePickerFragment();
        Bundle extras = new Bundle();
        long recordedAtInMilliSeconds = mRecordedAt.getMillis();
        extras.putLong(BundleKeys.DATE_TIME_IN_MILLISECONDS, recordedAtInMilliSeconds);
        dialogFragment.setArguments(extras);
        dialogFragment.setTargetFragment(this, NEW_REPORT_FRAGMENT_REQUEST_CODE);
        dialogFragment.show(getActivity().getSupportFragmentManager(),
                DateTimePickerFragment.FRAGMENT_TAG);
    }

    protected void onSubmitReport() {
        Editable waterLevel = waterLevelEditText.getText();
        Editable userName = userNameEditText.getText();
        Editable emailAddress = emailAddressEditText.getText();
        String recordedAt = DateTimeFormatter.getFormattedDateTime(
                mRecordedAt, BuildConfig.CROWD_SOURCING_GAUGE_REPORTS_API_DATE_TIME_PATTERN);
        // Save the user from retyping her contact information each time.
        mPreferencesHelper.storeUserName(userName.toString());
        mPreferencesHelper.storeEmailAddress(emailAddress.toString());
        int featureId = mSelectedGaugeItem.getFeatureId();
        GaugeReport gaugeReport = new GaugeReport(
                featureId,
                waterLevel.toString(),
                userName.toString(),
                emailAddress.toString(),
                recordedAt);
        createGaugeReport(gaugeReport);
    }

    protected void createGaugeReport(final GaugeReport gaugeReport) {
        TypedString typedGauge = new TypedString(gaugeReport.gauge);
        TypedString typedWaterLevel = new TypedString(gaugeReport.waterLevel);
        TypedString typedUserName = new TypedString(gaugeReport.userName);
        TypedString typedEmailAddress = new TypedString(gaugeReport.emailAddress);
        TypedString typedRecordedAt = new TypedString(gaugeReport.recordedAt);
        File gaugePhoto = new File(mGaugePhotoPath);
        TypedFile typedGaugePhoto = new TypedFile("multipart/form-data", gaugePhoto);

        Observable<CreateCrowdSourcingGaugeReportResponse> responseObservable =
                mGaugeReportsService.createCrowdSourcingGaugeReport(
                        typedGauge,
                        typedWaterLevel,
                        typedUserName,
                        typedEmailAddress,
                        typedRecordedAt,
                        typedGaugePhoto);
        mGaugeReportsSubscription = AppObservable.bindFragment(this, responseObservable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CreateCrowdSourcingGaugeReportResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.d(getClass().getName(), "Completed createGaugeReport");
                        String message = getString(R.string.new_report_submitted_successfully);
                        Crouton.makeText(getActivity(), message, Style.CONFIRM).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // TODO: Replace with meaningful, localized error messages.
                        if (e instanceof RetrofitError) {
                            RetrofitError retrofitError = (RetrofitError) e;
                            Crouton.makeText(getActivity(), retrofitError.getMessage(), Style.ALERT).show();
                        }
                        Log.e(getClass().getName(), e.getMessage());
                    }

                    @Override
                    public void onNext(CreateCrowdSourcingGaugeReportResponse createCrowdSourcingGaugeReportResponse) {
                        Log.d(getClass().getName(), createCrowdSourcingGaugeReportResponse.toString());
                    }
                });
    }

}
