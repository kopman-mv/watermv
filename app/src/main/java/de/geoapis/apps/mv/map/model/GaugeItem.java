package de.geoapis.apps.mv.map.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.parceler.Parcel;

@Parcel
public class GaugeItem implements ClusterItem {

    protected int mFeatureId;
    protected LatLng mPosition;
    protected String mGaugeCode;
    protected String mGaugeNumberWsa;
    protected String mGaugeName;

    protected String mAreaKeyLawa;
    protected String mWaterKeyLawa;
    protected String mWaterNameLavaRoute;

    protected String mGaugeDatum;
    protected String mGaugeDatumHeightSystem;

    protected String mLakeName;
    protected String mWaterTypeGauge;
    protected String mEasting;
    protected String mNorthing;

    protected String mDrainageArea;
    protected String mDirectDrainageArea;
    protected String mDirectDrainageAreaPercentual;

    protected String mTimeSeriesStart;
    protected String mTimeSeriesRange;

    // Pegelhauptwerte

    protected String mPrincipalValueNw;
    protected String mPrincipalValueMnw;
    protected String mPrincipalValueMw;
    protected String mPrincipalValueMhw;
    protected String mPrincipalValueHw;
    protected String mPrincipalValueNnw;
    protected String mPrincipalValueHhw;
    protected String mPrincipalValueNq;
    protected String mPrincipalValueMnq;
    protected String mPrincipalValueMq;
    protected String mPrincipalValueMhq;
    protected String mPrincipalValueHq;
    protected String mPrincipalValueNnq;
    protected String mPrincipalValueHhq;

    public GaugeItem() {
        // Required by Parceler
    }

    public GaugeItem(int featureId,
                     LatLng position,
                     String gaugeCode,
                     String gaugeName,
                     String gaugeNumberWsa,
                     String areaKeyLawa,
                     String waterKeyLawa,
                     String waterNameLavaRoute,
                     String gaugeDatum,
                     String gaugeDatumHeightSystem,
                     String lakeName,
                     String waterTypeGauge,
                     String easting,
                     String northing,
                     String timeSeriesStart,
                     String timeSeriesRange,
                     String principalValueNw,
                     String principalValueMnw,
                     String principalValueMw,
                     String principalValueMhw,
                     String principalValueHw,
                     String principalValueNnw,
                     String principalValueHhw,
                     String principalValueNq,
                     String principalValueMnq,
                     String principalValueMq,
                     String principalValueMhq,
                     String principalValueHq,
                     String principalValueNnq,
                     String principalValueHhq,
                     String drainageArea,
                     String directDrainageArea,
                     String directDrainageAreaPercentual
    ) {
        mFeatureId = featureId;
        mPosition = position;
        mGaugeCode = gaugeCode;
        mGaugeName = gaugeName;
        mGaugeNumberWsa = gaugeNumberWsa;
        mAreaKeyLawa = areaKeyLawa;
        mWaterKeyLawa = waterKeyLawa;
        mWaterNameLavaRoute = waterNameLavaRoute;
        mGaugeDatum = gaugeDatum;
        mGaugeDatumHeightSystem = gaugeDatumHeightSystem;
        mLakeName = lakeName;
        mWaterTypeGauge = waterTypeGauge;
        mEasting = easting;
        mNorthing = northing;
        mTimeSeriesStart = timeSeriesStart;
        mTimeSeriesRange = timeSeriesRange;
        mPrincipalValueNw = principalValueNw;
        mPrincipalValueMnw = principalValueMnw;
        mPrincipalValueMw = principalValueMw;
        mPrincipalValueMhw = principalValueMhw;
        mPrincipalValueHw = principalValueHw;
        mPrincipalValueNnw = principalValueNnw;
        mPrincipalValueHhw = principalValueHhw;
        mPrincipalValueNq = principalValueNq;
        mPrincipalValueMnq = principalValueMnq;
        mPrincipalValueMq = principalValueMq;
        mPrincipalValueMhq = principalValueMhq;
        mPrincipalValueHq = principalValueHq;
        mPrincipalValueNnq = principalValueNnq;
        mPrincipalValueHhq = principalValueHhq;
        mDrainageArea = drainageArea;
        mDirectDrainageArea = directDrainageArea;
        mDirectDrainageAreaPercentual = directDrainageAreaPercentual;
    }


    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public int getFeatureId() {
        return mFeatureId;
    }

    public String getGaugeCode() {
        return mGaugeCode;
    }

    public String getGaugeNumberWsa() {
        return mGaugeNumberWsa;
    }

    public String getGaugeName() {
        return mGaugeName;
    }

    public String getAreaKeyLawa() {
        return mAreaKeyLawa;
    }

    public String getWaterKeyLawa() {
        return mWaterKeyLawa;
    }

    public String getWaterNameLavaRoute() {
        return mWaterNameLavaRoute;
    }

    public String getGaugeDatum() {
        return mGaugeDatum;
    }

    public String getGaugeDatumHeightSystem() {
        return mGaugeDatumHeightSystem;
    }

    public String getLakeName() {
        return mLakeName;
    }

    public String getWaterTypeGauge() {
        return mWaterTypeGauge;
    }

    public String getEasting() {
        return mEasting;
    }

    public String getNorthing() {
        return mNorthing;
    }

    public String getTimeSeriesRange() {
        return mTimeSeriesRange;
    }

    public String getTimeSeriesStart() {
        return mTimeSeriesStart;
    }

    public String getPrincipalValueHhq() {
        return mPrincipalValueHhq;
    }

    public String getPrincipalValueHq() {
        return mPrincipalValueHq;
    }

    public String getPrincipalValueHw() {
        return mPrincipalValueHw;
    }

    public String getPrincipalValueMhq() {
        return mPrincipalValueMhq;
    }

    public String getPrincipalValueMhw() {
        return mPrincipalValueMhw;
    }

    public String getPrincipalValueMnq() {
        return mPrincipalValueMnq;
    }

    public String getPrincipalValueMnw() {
        return mPrincipalValueMnw;
    }

    public String getPrincipalValueMq() {
        return mPrincipalValueMq;
    }

    public String getPrincipalValueMw() {
        return mPrincipalValueMw;
    }

    public String getPrincipalValueNnq() {
        return mPrincipalValueNnq;
    }

    public String getPrincipalValueNnw() {
        return mPrincipalValueNnw;
    }

    public String getPrincipalValueNq() {
        return mPrincipalValueNq;
    }

    public String getPrincipalValueHhw() {
        return mPrincipalValueHhw;
    }

    public String getPrincipalValueNw() {
        return mPrincipalValueNw;
    }

    public String getDrainageArea() {
        return mDrainageArea;
    }

    public String getDirectDrainageArea() {
        return mDirectDrainageArea;
    }

    public String getDirectDrainageAreaPercentual() {
        return mDirectDrainageAreaPercentual;
    }

}
