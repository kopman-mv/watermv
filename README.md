# Waters Mecklenburg-Vorpommern

Android app for water information in Mecklenburg-Vorpommern


## Usage

Before you can install and run the application please add
the API key for Google Maps. The string must be defined here:

* app/src/debug/res/values/google_maps_api.xml
* app/src/release/res/values/google_maps_api.xml


### Backend configuration

Create a copy of `gradle.properties.example` and rename it to
`gradle.properties`. Then add the required settings for the
backend.


## Autors

* Tobias Preuss
